#BUILDER ======================================
# Fonte: http://tattoocoder.com/exploring-multistage-docker-builds-for-angular-apps/
FROM node:7-alpine as builder

ARG BUILD_ENV=dev
ARG BUILD_OPTS=
RUN npm install -g @angular/cli@1.6.8 --save

ENV APP_HOME=/usr/src/app
RUN mkdir -p ${APP_HOME}
WORKDIR ${APP_HOME}
COPY package.json ${APP_HOME}
RUN npm install
COPY . ${APP_HOME}

RUN ng build --env=${BUILD_ENV} ${BUILD_OPTS} --output-hashing=all

#SERVER =======================================
FROM nginx:alpine
COPY --from=builder /usr/src/app/dist /usr/share/nginx/html

# config carregada nos compose files
#COPY nginx-config/default.conf /etc/nginx/conf.d/default.conf

EXPOSE 80
EXPOSE 443
