interface ApiResponse {
    body: Array<Object>;
    message: string;
    token: string;
    status: number;
}

interface ApiResponseObj {
    body: Object;
    message: string;
    token: string;
    status: number;
}

interface Usuario {
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    orgao: string;
}

interface UsuarioOK {
    firstName: string;
    id: number;
    id_orgao: number;
    lastName: string;
    nome_orgao: string;
    username: string;
}

interface UsuarioCorrente {
    username: string;
    nomecompleto: string;
    orgao: string;
}

interface Claims {
    iss: string;
    iat: number;
    exp: number;
    aud: string;
    firstname: string;
    lastname: string;
    orgao: string;
    username: string;
}

// Tipos de dados para acesso a eventos via API

interface EventosApiDados {
    id: number;
    data_geracao: Date;
    nm_responsavel: string;
    id_situacao: number;
    id_evento: number;
    evento: string;
    tipo_evento: string;
    situacao_evento: string;
}

interface ApiResponseEventosPaginadosBoby {
    current_page: number;
    data: EventosApiDados[];
    first_page_url: string;
    from: number;
    next_page_url: string;
    path: string;
    per_page: string;
    prev_page_url: string;
    to: number;
    total: number;
}

interface ApiResponseEventosPaginados {
    body: ApiResponseEventosPaginadosBoby;
    message: string;
    token: string;
    status: number;
}

interface ApiResponseAny {
    body: any;
    message: string;
    token: string;
    status: number;
}

interface ApiRestHeartResponseBody {
    _embedded: any[];
    _id: string;
    _size: number;
    _total_pages: number;
    _returned: number;
}

interface ApiEventosResponse {
    body: ApiRestHeartResponseBody;
    headers: any;
    ok: boolean;
    status: number;
    statusText: string;
    type: number;
    url: string;
}
