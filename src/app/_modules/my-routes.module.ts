import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { MyContentComponent,
    MyDashboardComponent,
    MyLoginComponent,
    MyQualificacaoComponent,
    MyPendentesComponent,
    MyEventosTrabalhadorComponent,
    MyEnvioEventosComponent,
    MyFolhaPagamentoComponent,
    MyRelatoriosComponent,
    MyAgrupamentoFolhaComponent,
    MyDetalhePagamentoComponent,
    MyApuracaoConsolidadaImpostoRendaComponent,
    MyApuracaoConsolidadaFundoDeGarantiaComponent,
    MyApuracaoConsolidadaContribuicoesSociaisComponent,
    MyPainelComponent,
    MyConfigComponent,
    MyPainelDetalheComponent,
    MyPainelListComponent,
    MyRelatorioErrosComponent} from './../_components/index';

import { UnauthorizedComponent } from './../_misc/index';

import { MyAuthGuard } from './../_guards/index';
//eventos-trabalhador
const routes: Routes = [
    { path: '', component: MyDashboardComponent, canActivate: [MyAuthGuard] },
    { path: 'qualificacao', component: MyQualificacaoComponent, canActivate: [MyAuthGuard] },
    { path: 'pendentes', component: MyPendentesComponent, canActivate: [MyAuthGuard] },
    { path: 'eventos-trabalhador', component: MyEventosTrabalhadorComponent, canActivate: [MyAuthGuard] },
    { path: 'envio-eventos', component: MyEnvioEventosComponent, canActivate: [MyAuthGuard] },
    { path: 'pendentes:id', component: MyPendentesComponent, canActivate: [MyAuthGuard] },
    { path: 'folha-pagamento', component: MyFolhaPagamentoComponent, canActivate: [MyAuthGuard] },
    { path: 'relatorios', component: MyRelatoriosComponent, canActivate: [MyAuthGuard] },
    { path: 'detalhe-folha-pagamento/:idFolha', component: MyDetalhePagamentoComponent, canActivate: [MyAuthGuard] },
    { path: 'detalhe-folha-pagamento/:idFolha/:idEvento', component: MyDetalhePagamentoComponent, canActivate: [MyAuthGuard] },
    { path: 'agrupamento-folha-pagamento/:idFolha', component: MyAgrupamentoFolhaComponent, canActivate: [MyAuthGuard] },
    { path: 'apuracao-consolidada-imposto-renda/:idFolha', component: MyApuracaoConsolidadaImpostoRendaComponent, canActivate: [MyAuthGuard] },
    { path: 'apuracao-consolidada-fundo-de-garantia/:idFolha', component: MyApuracaoConsolidadaFundoDeGarantiaComponent, canActivate: [MyAuthGuard] },
    { path: 'apuracao-consolidada-contribuicoes-sociais/:idFolha', component: MyApuracaoConsolidadaContribuicoesSociaisComponent, canActivate: [MyAuthGuard] },
    { path: 'relatorio-erros', component: MyRelatorioErrosComponent, canActivate: [MyAuthGuard] },
    { path: 'config', component: MyConfigComponent, canActivate: [MyAuthGuard] },
    { path: 'login', component: MyLoginComponent },
    { path: 'unauthorized', component: UnauthorizedComponent},

    {
        path: 'painel',
        component: MyPainelComponent,
        canActivate: [MyAuthGuard],
        children: [
            {
                path: '',
                component: MyPainelListComponent,
            },
            {
                path: ':id',
                component: MyPainelDetalheComponent
            }
]
    },

    { path: '**', redirectTo: '' }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ],
    declarations: []
})
export class MyRoutesModule { }
