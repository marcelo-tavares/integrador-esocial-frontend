export const MyConstantes = {
    IMPORTADOS: 0,
    COM_ENVIO_AUTORIZADO: 1,
    AGUARDANDO_PROCESSAMENTO_ESOCIAL: 2,
    RECEBIDOS_COM_SUCESSO: 3,
    COM_ERRO: 4
};

export const MyVersion = {
    MAJOR: 2,
    MINOR: 0,
    PATCH: 2,
    DATE: '16/10/2018'
};

/*
* Versionamento
* 2.0.2 - 16/10/2018 - Alterada a porta do serviço https
* 2.0.1 - 10/10/2018 - Alterado para usar https
* 2.0.0 - 05/09/2018 - Alterados metodos de autenticação
* 1.7.1 - 27/08/2018 - DW-62 - Melhorado visualização do XML no Painel de Controle
* 1.7.0 - 23/08/2018 - DW-69 - Incluído tipo de erro na planilha de erros da folha
* 1.6.2 - 21/08/2018 - Adicionado parametro de sort na busca de eventos da folha
*                    - Adicionado tratamento de erro ao carregar eventos da folha
* 1.6.1 - 17/08/2018 - DW-77 - Permitir fechar folha reaberta ou reaberta com erro
* 1.6.0 - 17/08/2018 - DW-62 - Melhorada visualização do XML com PrismJS
* 1.5.1 - 16/08/2018 - DW-53 - Adicionada cor no tipo da ocorrencia na lista de ocorrencia dos eventos
* 1.5.0 - 15/08/2018 - DW-60 - Criados logos para SCPar, Santur e Cohab
* 1.4.0 - 14/08/2018 - DW-53 - Adicionado o tipo da ocorrencia na lista de ocorrencias dos eventos
* 1.3.1 - 13/08/2018 - DW-63 - Corrigido bug que permitia transmitir eventos de folha já com sucesso
* 1.3.0 - 09/08/2018 - DW-41 - Permitir reenviar eventos com erro
* 1.2.2 - 08/08/2018 - DW-54 - Corrigido bug que permitia transmitir eventos 1298 e 1299
* 1.2.1 - 07/08/2018 - DW-52 - Removido o CDN do bootstrap do index.html
* 1.2.0 - 06/08/2018 - DW-50 - Adicionado indicador de evento excluído do eSocial
* 1.1.2 - 06/08/2018 - DW-49 - Padronização dos botões de excluir
* 1.1.1 - 06/08/2018 - ESOCIAL-141 - Removida quantidade de CPFs do relatório de erros
* 1.1.0 - 03/08/2018 - DW-44 - Dialogs de confirmação ao importar e transmitir folha
* */

