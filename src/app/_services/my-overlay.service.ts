import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class MyOverlayService {
    private subject = new Subject<any>();
    constructor() { }

    mostrar() {
        this.subject.next(true);
    }

    ocultar() {
        this.subject.next(false);
    }

    getMessage(): Observable<any> {
        return this.subject.asObservable();
    }
}
