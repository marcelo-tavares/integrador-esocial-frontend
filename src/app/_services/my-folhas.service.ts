import { Injectable } from '@angular/core';
import { MyBackendRoutes } from '../_config';
import { MyLocalStorageService } from './my-localstorage.service';
import { MyHttpService } from './my-http.service';
import { MySituacaoFolha, MyFolhaPagamento} from '../_components/my-folha-pagamento/my-folha-pagamento.class';
import {MyESocialReturnCodes} from "../_config/my-eSocialReturnCodes.config";


@Injectable()
export class MyFolhasService {

    constructor(
        private _myLocalStorageService: MyLocalStorageService,
        private _myHttpService: MyHttpService
    ) { }

    getFolhas(page: number = 1, pagesize: number = 10) {

        const paginacaoStr = '?page=' + page + '&pagesize=' + pagesize;
        const sortStr = '&sort_by=-_id';
        const optionsStr = '&hal=f&count';
        const path = this._myLocalStorageService.getBaseUrl() + MyBackendRoutes.FOLHAS + paginacaoStr + optionsStr + sortStr;
        return this._myHttpService.get(path);
    }

    getFolhasTotalizadores(folha) {

        const avars  = "?avars={'folha':'" + folha + "'}";

        const path = this._myLocalStorageService.getBaseUrl() + MyBackendRoutes.FOLHAS_AGGRS + avars;
        return this._myHttpService.get(path);
    }

    getBaseFgts(id){
        const avars  = "?filter={'id':" + id + "}";
        const path = MyBackendRoutes.BASEFGTS + avars;
        return this._myHttpService.get(path);
    }

    getInfoContrib(id){
        const avars  = "?filter={'id':" + id + "}";
        const path = MyBackendRoutes.CLASSTRIBUT + avars;
        return this._myHttpService.get(path);
    }

    getCategTrab(id){
        const avars  = "?filter={'id':" + id + "}";
        const path = MyBackendRoutes.CATEGTRAB + avars;
        return this._myHttpService.get(path);
    }

    getDpsFgts(id){
        const avars  = "?filter={'id':" + id + "}";
        const path = MyBackendRoutes.DPSFGTS + avars;
        return this._myHttpService.get(path);
    }

    getFolha(idFolha: string) {
        const path = this._myLocalStorageService.getBaseUrl() + MyBackendRoutes.FOLHAS + idFolha;
        return this._myHttpService.get(path);
    }

    getErrosFolha(idFolha: string, pagina = 1) {
        const filtro = `?avars={"folha":"${idFolha}"}`;
        const page = `&page=${pagina}`;
        const pagesize = `&pagesize=1000`;
        const path = this._myLocalStorageService.getBaseUrl() + MyBackendRoutes.ERROS_FOLHA + filtro + page + pagesize;
        return this._myHttpService.get(path);
    }

    getSituacoesFolha() {
        const path = MyBackendRoutes.SITUACOES_FOLHA;
        return this._myHttpService.get(path);
    }

    atualizaSituacaoFolha(folha: MyFolhaPagamento, novaSituacao: MySituacaoFolha) {
        const path = this._myLocalStorageService.getBaseUrl() + MyBackendRoutes.FOLHAS + folha._id;
        const payload = {
            status: novaSituacao,
            remuneracao: folha.remuneracao,
            pagamento: folha.pagamento,
            prodRural: folha.prodRural,
            comProducao: folha.comProducao,
            trabNaoPortuario: folha.trabNaoPortuario,
            infComplementar: folha.infComplementar
        };
        return this._myHttpService.patch(path, payload);
    }

    transmiteEventosFolha(folha: MyFolhaPagamento) {
        // Estes eventos nao devem ser transmitidos
        const notTipos = ['S-1298', 'S-1299'];
        const notTiposArray = notTipos.map(tipo => `{"tipo.id":{"$ne":"${tipo}"}}`);

        // Não transmitir eventos com sucesso
        const notStatus = [MyESocialReturnCodes.SUCESSO, MyESocialReturnCodes.SUCESSO_ADVERTENCIA];
        const notStatusArray = notStatus.map(status => `{"status.cdResposta":{"$ne":${status}}}`);

        const arrayExcecoes = [...notTiposArray, ...notStatusArray];

        const filtro = `*?filter={"$and":[{"folha":{"$in":["${folha._id}"]}},${arrayExcecoes.toString()}]}`;

        const payload = {
            status: {
                cdResposta: 101,
                descResposta: 'Envio para o eSocial autorizado'
            }
        };

        const path = this._myLocalStorageService.getBaseUrl() + MyBackendRoutes.EVENTOS + filtro;
        return this._myHttpService.patch(path, payload);
    }
}
