import { Injectable } from '@angular/core';

import * as _ from 'underscore';


@Injectable()
export class MyArrayService {

    constructor() { }

    find(myObj: any, myArray: any[]): boolean {
        const x = _.filter(myArray, myObj);
        if (x.length) {
            return true;
        } else {
            return false;
        }
    }
}
