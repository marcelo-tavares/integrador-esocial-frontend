import { Injectable } from '@angular/core';

import * as _ from 'underscore';
import './../_types/my_types';



@Injectable()
export class MyScopeService {

    filtrarImportados: boolean;
    filtrarAutorizados: boolean;
    filtrarEnviados: boolean;
    filtrarErros: boolean;

    usuarioCorrente: UsuarioCorrente;
    orgaoImagemLogo: string;
    orgaoURLREST: string;

    constructor() {
        this.clearFiltrar();
        this.setUsuarioCorrente('', '', '', '');
     }

     clearFiltrar() {
         this.filtrarImportados =
         this.filtrarAutorizados =
         this.filtrarEnviados =
         this.filtrarErros = false;
     }

    find(myObj: any, myArray: any[]): boolean {
        const x = _.filter(myArray, myObj);
        if (x.length) {
            return true;
        } else {
            return false;
        }
    }

    getNomeCompleto(first: string, last: string) {
        return first + ' ' + last;
    }

    setUsuarioCorrente(username, orgao, nome, sobrenome) {
        this.usuarioCorrente = {
            username: username,
            orgao: orgao,
            nomecompleto: this.getNomeCompleto(nome, sobrenome)
        };

        this.orgaoImagemLogo = orgao.charAt(0).toUpperCase() + orgao.slice(1).toLowerCase();
        this.orgaoURLREST = orgao.toLowerCase();
    }

    getBaseUrl () {
        return '/' + this.orgaoURLREST + '/';
    }
}
