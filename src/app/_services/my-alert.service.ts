import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class MyAlertService {
    private subject = new Subject<any>();
    public manterAposMudarRotaDefault = false;

    public MANTER_ALERTA_APOS_NAVEGACAO = true;

    constructor() { }

    getMessage(): Observable<any> {
        return this.subject.asObservable();
    }

    success(message: string, manterAposMudarRota = false) {
        this.manterAposMudarRotaDefault = manterAposMudarRota;
        this.subject.next({ type: 'success', text: message });
    }

    warning(message: string, manterAposMudarRota = false) {
        this.manterAposMudarRotaDefault = manterAposMudarRota;
        this.subject.next({ type: 'warning', text: message });
    }

    error(message: string, manterAposMudarRota = false) {
        this.manterAposMudarRotaDefault = manterAposMudarRota;
        this.subject.next({ type: 'danger', text: message });
    }

    clear() {
        this.subject.next();
        this.manterAposMudarRotaDefault = false;
    }

    clearAfterNavigation() {
        if (this.manterAposMudarRotaDefault) {
            this.manterAposMudarRotaDefault = false;
        } else {
            // Limpar alerta
            this.clear();
        }

    }
}
