import { Injectable } from '@angular/core';

@Injectable()
export class MyLocalStorageService {

    constructor() { }

    save(variavel, valor: string) {
        localStorage.setItem(variavel, valor);
    }

    get(variavel): string {
        return localStorage.getItem(variavel);
    }

    delete(variavel) {
        localStorage.removeItem(variavel);
    }

    todasMinusculas(string) {
        return string.toLowerCase();
    }

    primeiraMaiuscula(string) {
        return string.charAt(0).toUpperCase() + this.todasMinusculas( string.slice(1));
    }

    getBaseUrl() {
        return '/' + atob(this.get('epgd')).toLowerCase() + '/';
    }
}
