import {
    HttpErrorResponse,
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpRequest,
    HttpResponse
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
    Router,
    RouterState,
    RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';

import { MyTokenService } from './my-token.service';
import { MyOverlayService } from './my-overlay.service';
import { MyAlertService } from './my-alert.service';

@Injectable()
export class MyAuthInterceptor implements HttpInterceptor {

    constructor(
        private _myTokenService: MyTokenService,
        private _myOverlayService: MyOverlayService,
        private _myAlertService: MyAlertService,
        private router: Router) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token = this._myTokenService.getToken();

        let newReq = req;

        // adiciona o token header ao request
        if (token) {
            newReq = req.clone({ setHeaders: { Authorization: 'Bearer ' + token } });
        } else {
            this.router.navigate(['/login']);
        }

        return next.handle(newReq)
            .do(event => { // trata a resposta quando o status for 401 ou 403 para redirecionar para pagina de erro
                if (event instanceof HttpResponse) {
                    // console.log('Resposta HTTP com status: ' + event.status);
                    if (event.status === 401) {
                        this.sessaoInvalida();
                    } else if (event.status === 403) {
                        this.recursoNaoAutorizado();
                    }
                }
            },
            error => {
                // console.log('Erro capturado: ' + error);

                if (error instanceof HttpErrorResponse) {
                    // console.log('Resposta HTTP com status * : ' + error.status);
                    if (error.status === 401) {
                        this.sessaoInvalida();
                    } else if (error.status === 403) {
                        this.recursoNaoAutorizado();
                    }
                }
            });
    }

    // TODO: Estas funções estão repetidas aqui e em my-auth.service.ts
    sessaoInvalida() {
        const state: RouterState = this.router.routerState;
        const snapshot: RouterStateSnapshot = state.snapshot;

        this._myTokenService.deleteToken();
        this._myAlertService.error('Sua sessão não está mais válida. Por favor faça nova autenticação.',
            this._myAlertService.MANTER_ALERTA_APOS_NAVEGACAO);
        this._myOverlayService.ocultar();
        this.router.navigate(['/login'], { queryParams: { returnUrl: snapshot.url }});
    }

    recursoNaoAutorizado() {
        this._myAlertService.error('Você não tem autorização para acessar esse recurso.',
            this._myAlertService.MANTER_ALERTA_APOS_NAVEGACAO);
        this._myOverlayService.ocultar();
        this.router.navigate(['/unauthorized']);
    }

}
