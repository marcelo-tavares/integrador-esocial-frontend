import { Injectable } from '@angular/core';

@Injectable()
export class MyTokenService {

    constructor() {}

    genToken(u: string, p: string) {
        return 'Basic ' + btoa(u + ':' + p);
    }

    saveToken(token: string) {
        localStorage.setItem('token', token);
    }

    getToken(): string {
        return localStorage.getItem('token');
    }

    deleteToken() {
        localStorage.removeItem('token');
    }
}
