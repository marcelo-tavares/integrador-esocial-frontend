import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import { MyTokenService } from './my-token.service';
import {MyJwtHelper} from './../_helpers/my-jwtHelper';


@Injectable()
export class MyMenuService {
    private subject = new Subject<boolean>();

    constructor() {}

    mostrar() {
        this.subject.next(true);
    }

    ocultar() {
        this.subject.next(false);
    }

    getMessage(): Observable<any> {
        return this.subject.asObservable();
    }
}
