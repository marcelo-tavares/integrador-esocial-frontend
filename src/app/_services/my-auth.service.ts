import { Injectable } from '@angular/core';
import {
    Router,
    RouterState,
    RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

import { MyJwtHelper } from './../_helpers/index';
import { MyHttpService } from './my-http.service';
import { MyTokenService } from './my-token.service';
import { MyAlertService } from './my-alert.service';
import { MyOverlayService } from './my-overlay.service';
import { MyLocalStorageService } from './my-localstorage.service';
import { MyBackendRoutes } from './../_config/index';


import './../_types/my_types';

@Injectable()
export class MyAuthService {

    public nomeCompleto: string;
    public orgao: string;
    private usuarioOk: UsuarioOK;

    constructor(private _myHttpService: MyHttpService,
                private _myTokenService: MyTokenService,
                private _myAlertService: MyAlertService,
                private _myOverlayService: MyOverlayService,
                private _myLocalStorageService: MyLocalStorageService,
                private _router: Router,
                private _myJwtHelper: MyJwtHelper) {
    }

    login(username: string, password: string): any  {
        // Gera token de login com usuário e senha
        const authTokenLogin = this._myTokenService.genToken(username, password);
        return this._myHttpService.doLogin(authTokenLogin, username).map(data => data);
    }

    logout() {
        this._myTokenService.deleteToken();
        this._myLocalStorageService.delete('epgd');
        this._myLocalStorageService.delete('usrnm');
    }

    loggedIn() {
        return (this._myTokenService.getToken()) ? true : false;
    }

    checkAuth() {
        if (!this._myHttpService.checkAuth(this._myTokenService.getToken())){
            this.sessaoInvalida();
        }
    }

    sessaoInvalida() {
        const state: RouterState = this._router.routerState;
        const snapshot: RouterStateSnapshot = state.snapshot;

        this._myTokenService.deleteToken();
        this._myAlertService.error('Sua sessão não está mais válida. Por favor faça nova autenticação.',
            this._myAlertService.MANTER_ALERTA_APOS_NAVEGACAO);
        this._myOverlayService.ocultar();
        this._router.navigate(['/login'], { queryParams: { returnUrl: snapshot.url }});
    }

    recursoNaoAutorizado() {
        this._myAlertService.error('Você não tem autorização para acessar esse recurso.',
            this._myAlertService.MANTER_ALERTA_APOS_NAVEGACAO);
        this._myOverlayService.ocultar();
        this._router.navigate(['/unauthorized']);
    }


    // getNomeCompleto(first: string, last: string) {
    //     return first + ' ' + last;
    // }

    // getCurrentUser() {
    //     const token = this._myTokenService.getToken();

    //     if (token) {
    //         // const  decoded = this._myJwtHelper.decodeToken(token);

    //         const user = {
    //             username: 'marcelo' ,
    //             nomecompleto: this.getNomeCompleto('Marcelo', 'Tavares'),
    //             orgao: 'CIASC'
    //         };

    //         // const user = {
    //         //     username: decoded.username,
    //         //     nomecompleto: this.getNomeCompleto(decoded.firstname, decoded.lastname),
    //         //     orgao: decoded.orgao
    //         // };
    //         return user;
    //     } else {
    //         return null;
    //     }
    // }
}
