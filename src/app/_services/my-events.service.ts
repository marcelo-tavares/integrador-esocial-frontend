import { Injectable } from '@angular/core';
import { MyBackendRoutes } from '../_config';
import { MyLocalStorageService } from './my-localstorage.service';
import { MyHttpService } from './my-http.service';

export class MyEventFilter {
    folha: string = null;
    tipos: any[] = [];
    notTipos: any[] = [];
    grupos: any[] = [];
    notGrupos: any[] = [];
    statuses: any[] = [];
    notStatuses: any[] = [];
    operacoes: any[] = [];
    notOperacoes: any[] = [];
    pesquisaTextual: string;
}

@Injectable()
export class MyEventsService {

    constructor(
        private _myLocalStorageService: MyLocalStorageService,
        private _myHttpService: MyHttpService
    ) { }

    getEventos(filtro: MyEventFilter, page: number = 1, pagesize: number = 10) {

        const paginacaoStr = '?page=' + page + '&pagesize=' + pagesize;
        // const sortStr = '&sort={"tipo.id":1, "dhGeracao":-1}'; // Ordenacao quebra o backend em listas muito grandes
        const optionsStr = '&hal=f&count';
        const filterStr = this.montaStringFiltros(filtro);

        const path = this._myLocalStorageService.getBaseUrl() + MyBackendRoutes.EVENTOS + paginacaoStr + filterStr + optionsStr;
        return this._myHttpService.get(path);
    }

    getEventosFolhaPagamento(filtro: MyEventFilter, page: number = 1, pagesize: number = 10) {

        const paginacaoStr = '?page=' + page + '&pagesize=' + pagesize;
        // const sortStr = '&sort={"tipo.id":1, "dhGeracao":-1}'; // Ordenacao quebra o backend em listas muito grandes
        const sortStr = '&sort_by={}'; // TODO: alterar o sort quando for resolvida a view
        const optionsStr = '&hal=f&count';
        const filterStr = this.montaStringFiltros(filtro);

        const path = this._myLocalStorageService.getBaseUrl() + MyBackendRoutes.EVENTOS_FOLHA + paginacaoStr + filterStr + optionsStr + sortStr;
        return this._myHttpService.get(path);
    }

    removeEventos(eventos: any[]) {
        const arrayEventos = eventos.map(evento => `{_id:{$oid:"${evento._id.$oid}"}}`);

        if (arrayEventos.length > 0) {
            const stringEventos = '*?filter={$or:[' + arrayEventos.toString() + ']}';
            const path = this._myLocalStorageService.getBaseUrl() + MyBackendRoutes.EVENTOS + stringEventos;
            return this._myHttpService.delete(path);
        }

        return null;
    }

    enviaEventosESocial(eventos: any[]) {
        const arrayEventos = eventos.map(evento => `{_id:{$oid:"${evento._id.$oid}"}}`);

        if (arrayEventos.length > 0) {
            const payload = {
                status: {
                    cdResposta: 101,
                    descResposta: 'Envio para o eSocial autorizado'
                }
            };

            const stringEventos = '*?filter={$or:[' + arrayEventos.toString() + ']}';
            const path = this._myLocalStorageService.getBaseUrl() + MyBackendRoutes.EVENTOS + stringEventos;
            return this._myHttpService.patch(path, payload);
        }

        return null;
    }

    excluiEventoESocial(evento: any) {
        const path = this._myLocalStorageService.getBaseUrl() + MyBackendRoutes.EXCLUSAO_ESOCIAL;

        const payload = this.getPayloadExclusaoEvento(evento);

        return this._myHttpService.post(path, payload);
    }

    excluirEventosESocial(eventos: any[]) {
        if (eventos.length > 0) {
            const path = this._myLocalStorageService.getBaseUrl() + MyBackendRoutes.EXCLUSAO_ESOCIAL;
            const payload = [];
            eventos.forEach(evento => {
                payload.push(this.getPayloadExclusaoEvento(evento));
            });              
            return this._myHttpService.post(path, payload);
        }
        return null;
    }

    private getPayloadExclusaoEvento(evento: any) {
        return {
            xml: evento.xml,
            chave: evento.chave,
            operacao: {
                id: 3,
                descricao: 'Exclusão'
            },
            tipo: {
                id: evento.tipo.id,
                descricao: evento.tipo.descricao
            },
            grupo: {
                id: evento.grupo.id,
                descricao: evento.grupo.descricao
            },
            status: {
                cdResposta: 99,
                descResposta: 'Importação pendente'
            },
            eventoOriginal: {
                $oid: evento._id.$oid
            }
        }
    }

    getTiposEvento() {
        const path = MyBackendRoutes.LAYOUTS + '?sort=id';
        return this._myHttpService.get(path);
    }

    getSituacoesEvento() {
        const path = MyBackendRoutes.SITUACOES + '?sort=id';
        return this._myHttpService.get(path);
    }

    getOperacoesEvento() {
        const path = MyBackendRoutes.OPERACOES + '?sort=id';
        return this._myHttpService.get(path);
    }

    getRelatorioErros() {
        const path = this._myLocalStorageService.getBaseUrl() + MyBackendRoutes.RELATORIO_ERROS;
        return this._myHttpService.get(path);
    }

    private montaStringFiltros(filtro: MyEventFilter) {
        let arrayFiltros = [];

        if (filtro.pesquisaTextual && filtro.pesquisaTextual !== '') {
            arrayFiltros.push('{ "xml": { $regex: /' + filtro.pesquisaTextual + '/, $options: "sim"}}');
        }

        arrayFiltros = [...arrayFiltros,
            ...this.montaArrayFolha(filtro),
            ...this.montaArrayFiltroGrupo(filtro),
            ...this.montaArrayFiltroOperacao(filtro),
            ...this.montaArrayFiltroStatus(filtro),
            ...this.montaArrayFiltroTipo(filtro)];

        return arrayFiltros.length > 0 ? '&filter={"$and":[' + arrayFiltros.toString() + ']}' : '';
    }

    private montaArrayFolha(filtro: MyEventFilter): string[] {
        const folhas = [];
        if (filtro.folha) {
            folhas.push('{"folha":{"$in":["' + filtro.folha + '"]}}');
        }
        return folhas;
    }

    private montaArrayFiltroTipo(filtro: MyEventFilter): string[] {
        const notTipos = filtro.notTipos.map(tipo => `{"tipo.id": {"$ne":"${tipo.id}"}}`);
        const tipos = [];
        if (filtro.tipos.length > 0) {
            const idTipos = filtro.tipos.map(tipo => `"${tipo.id}"`);
            tipos.push('{"tipo.id":{"$in":[' + idTipos.toString() + ']}}');
        }

        return [...notTipos, ...tipos];
    }

    private montaArrayFiltroStatus(filtro: MyEventFilter): string[] {
        const notStatuses = filtro.notStatuses.map(status => '{"status.cdResposta": {"$ne":' + status.id + '}}');
        const statuses = [];
        if (filtro.statuses.length > 0) {
            if (filtro.statuses.findIndex(s => s.id === 300) >= 0) {// Se a opção "Somente eventos com erro" está na lista
                statuses.push('{"status.cdResposta":{"$gt":300}}');
            } else {
                const idStatuses = filtro.statuses.map(status => `${status.id}`);
                statuses.push(`{"status.cdResposta":{"$in":[${idStatuses.toString()}]}}`);
            }
        }

        return [...notStatuses, ...statuses];
    }

    private montaArrayFiltroGrupo(filtro: MyEventFilter): string[] {
        const notGrupos = filtro.notGrupos.map(grupo => '{"grupo.id": {"$ne":' + grupo.id + '}}');
        const grupos = [];
        if (filtro.grupos.length > 0) {
            const idGrupos = filtro.grupos.map(grupo => `${grupo.id}`);
            grupos.push('{"grupo.id":{"$in":[' + idGrupos.toString() + ']}}');
        }

        return [...notGrupos, ...grupos];
    }

    private montaArrayFiltroOperacao(filtro: MyEventFilter): string[] {
        const notOperacoes = filtro.notOperacoes.map(operacao => '{"operacao.id": {"$ne":' + operacao.id + '}}');
        const operacoes = [];
        if (filtro.operacoes.length > 0) {
            const idOperacoes = filtro.operacoes.map(operacao => `${operacao.id}`);
            operacoes.push('{"operacao.id":{"$in":[' + idOperacoes.toString() + ']}}');
        }

        return [...notOperacoes, ...operacoes];
    }

}
