import { Injectable } from '@angular/core';

import { MyLocalStorageService } from './my-localstorage.service';
import { MyHttpService } from './my-http.service';
import { MyBackendRoutes } from '../_config/index';
import { MyAlertService } from './my-alert.service';

@Injectable()
export class MyActiveUserService {

    constructor(
        private _myLocalStorageService: MyLocalStorageService,
        private _myAlertService: MyAlertService,
        private _myHttpService: MyHttpService) { }

    get() {
        const usrnm = atob(this._myLocalStorageService.get('usrnm'));
        const path = MyBackendRoutes.NOMECOMPLETO + usrnm;

        this._myHttpService.get(path)
            .subscribe((response: any) => {
                if (response.status === 200) {
                    const nmcplt = btoa(response.body.nome_usuario);
                    this._myLocalStorageService.save('nmcplt', nmcplt);
                } else {
                    this._myAlertService.warning('Erro ao recuperar dados do usuario');
                }
            });

    }


}
