import { Injectable } from '@angular/core';

@Injectable()
export class MyValidateService {

  constructor() { }

  isZero(valor) {
    if (valor === 0) {
      return true;
    } else {
      return false;
    }
  }

  isNull(valor) {
    if (valor == null) {
      return true;
    } else {
      return false;
    }
  }

  isEmpty(valor: string) {
    if (valor.length === 0) {
      return true;
    } else {
      return false;
    }
  }
}
