import { Inject, Injectable } from '@angular/core';
import { HttpClient,
         HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import {
    APP_CONFIG,
    AppConfig } from './../app.config';

import { MyBackendRoutes } from './../_config/index';
import { MyScopeService } from './my-scope.service';
import {MyJwtHelper} from "../_helpers/my-jwtHelper";
import * as moment from 'moment';
import {MyTokenService} from "./my-token.service";

@Injectable()
export class MyHttpService {

    private apiURL: string;

    /**
      * Método construtor
      * @param httpClient: HttpClient
      */
    constructor(private _httpService: HttpClient,
                private _myScopeService: MyScopeService,
                private _myJwtHelper: MyJwtHelper,
                private _myTokenService: MyTokenService,
                @Inject(APP_CONFIG) _config: AppConfig) {

                    this.apiURL = _config.BACKEND_URL;

                }

    /**
      * Método url
      * @param path: string
      */
    url(path: string) {
        return (this.apiURL + path) as string;
    }

    // Autenticação e afins

    doLogin(authTokenLogin: string, username: string): Observable<any> {
        const path = MyBackendRoutes.SESSAO;
        const headers = new HttpHeaders().set('Authorization', authTokenLogin);
        return this._httpService.post(this.url(path), {}, {headers: headers,  observe: 'response'});
    }

    checkAuth(authTokenCheckAuth: string): boolean {
        const expiration = this._myJwtHelper.decodeToken(authTokenCheckAuth).exp;
        return moment().isBefore(expiration);
        // let username = atob(authTokenCheckAuth.slice(6));
        // const doisPontos = username.indexOf(':');
        // username = username.substr(0, doisPontos);
        // const path = MyBackendRoutes.AUTH + username;
        // const headers = new HttpHeaders().set('Authorization', authTokenCheckAuth);
        // return this._httpService.get(this.url(path), {headers: headers,  observe: 'response'})
        //             .map(data => data);
    }

    /**
      * Método get
      * @param path: string
      */
      get(path: string): Observable<Object> {
        return this._httpService.get(this.url(path), { observe: 'response'});
    }

    /**
      * Método patch
      * @param path: string
      * @param payload: any (json object)
      */
      patch(path: string, payload: any) {
        return this._httpService.patch(this.url(path), payload, { observe: 'response'});
    }

    /**
      * Método post
      * @param post: string
      * @param payload: any (json object)
      */
      post(path: string, payload: any) {
        return this._httpService.post(this.url(path), payload, { observe: 'response'});
    }

    /**
      * Método put
      * @param put: string
      * @param payload: any (json object)
      */
     put(path: string, payload: any) {
        return this._httpService.put(this.url(path), payload, { observe: 'response'});
    }

    /**
      * Método delete
      * @param path: string
      */
      delete(path: string) {
        return this._httpService.delete(this.url(path), { observe: 'response'});
    }
}
