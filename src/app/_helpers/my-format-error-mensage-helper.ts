import {Injectable} from "@angular/core";

@Injectable()
export class MyFormatErrorMensageHelper{

    public formataMensagemErro(item){
        if (item.descricao.search(/cvc-complex-type/g) != -1){
            
            var msg = '';
            
            var erros = item.descricao.split('is expected.');
            
            erros.forEach(element => {

                var value =  /':(\w+)}/.exec(element);

                if (value != null){
                    msg += " '" + value[1] +"' ";
                } 
            });
           
            item.descricao = "O(s) seguinte(s) elemento(s) não está(ão) preenchido(s): " + msg;
            
        }else if (item.descricao.search(/cvc-pattern-valid/g) != -1){

            var msg = '';

            var erros = item.descricao.split('is not valid.');

            erros.forEach(element => {

                var value =  /value '(\w+)/.exec(element);
                var e =  /element '(\w+)/.exec(element); 

                if (value != null){
                    msg += "O valor '" + value[1] + "' do elemento '" + e[1] + "' não é valido \n"
                } 
            });

           
            item.descricao = msg;
            
        }

    }
}