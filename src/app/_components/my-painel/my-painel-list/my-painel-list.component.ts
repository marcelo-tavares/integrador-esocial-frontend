import { Component, OnInit } from '@angular/core';

import {
    MyAuthService,
    MyHttpService,
    MyAlertService,
    MyOverlayService,
    MyScopeService,
    MyLocalStorageService} from './../../../_services/index';
import { Router, ActivatedRoute } from '@angular/router';

import { MyBackendRoutes } from './../../../_config/index';
import { MyESocialReturnCodes } from './../../../_config/index';

import * as _ from 'underscore';

@Component({
    templateUrl: './my-painel-list.component.html',
    styleUrls: ['./my-painel-list.component.css']
})
export class MyPainelListComponent implements OnInit {

    eventosAgregadosPipeline: any;
    totaisTipoSituacao = [];
    tiposEvento = [];
    gruposEvento = [];
    gruposEventosDescricao = [];


    constructor(
        private _myOverlayService: MyOverlayService,
        private _myAlertService: MyAlertService,
        private _myHttpService: MyHttpService,
        private _myScopeService: MyScopeService,
        private _myLocalStorageService: MyLocalStorageService,
        private _router: Router) {
    }

    ngOnInit() {
        this.getEventosPainel();
    }

    montarFiltros() {
        return '';
    }

    getEventosPainel() {
        const optionsStr = '?hal=f&count';
        const filterStr = this.montarFiltros();

        const path = this._myLocalStorageService.getBaseUrl() + MyBackendRoutes.PAINEL + filterStr + optionsStr;
        this._myOverlayService.mostrar();
        this._myAlertService.clear();
        this._myHttpService.get(path)
            .subscribe((response: any) => {
                if (response.status === 200) {
                    if (response.body._size > 0) {
                        this.totalizarPorTipoSituacao(response.body._embedded['rh:doc']);
                    } else {
                        this._myAlertService.warning('Atenção: Nenhum evento foi encontrado para os critérios informados.');
                    }
                } else {
                    this._myAlertService.warning('response.status <> 200');
                }
                this._myOverlayService.ocultar();
            });
    }

    totalizarPorTipoSituacao(array) {
        let grp: number;

        const eventoSituacao = [];

        array.forEach(element => {
            switch (element._id.status_cdResposta) {
                case 100:
                case 101:
                    grp = 0;
                    break;
                case 201:
                case 202:
                    grp = 1;
                    break;
                case 102:
                    grp = 2;
                    break;
                default:
                    grp = 3;
                    break;
            }

            if (this.totaisTipoSituacao[element._id.tipo_id] === undefined) {
                this.totaisTipoSituacao[element._id.tipo_id] = [];
                this.totaisTipoSituacao[element._id.tipo_id][0] = 0;
                this.totaisTipoSituacao[element._id.tipo_id][1] = 0;
                this.totaisTipoSituacao[element._id.tipo_id][2] = 0;
                this.totaisTipoSituacao[element._id.tipo_id][3] = 0;
            }

            this.totaisTipoSituacao[element._id.tipo_id][grp] += element.quantidade;
            this.totaisTipoSituacao[element._id.tipo_id]['operacao'] = element._id.operacao;
            this.totaisTipoSituacao[element._id.tipo_id]['grupo'] = element._id.grupo;
            this.totaisTipoSituacao[element._id.tipo_id]['tipo_desc'] = element._id.tipo_desc;

            this.tiposEvento.push(element._id.tipo_id);
        });

        // Usando a biblioteca underscore para eliminar duplicados do array e ordenar
        this.tiposEvento = _.sortBy(_.uniq(this.tiposEvento));
    }

    mostrarDetalhe(recebidos) {
        if (recebidos > 0) {
            return true;
        } else {
            return false;
        }

    }

    showDetalhes(eventoId) {
        const rota = '/painel/' + eventoId;
        this._router.navigate([rota]);
    }
}
