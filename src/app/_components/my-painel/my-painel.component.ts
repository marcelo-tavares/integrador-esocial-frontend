import { Component, OnInit } from '@angular/core';

import { MyAuthService } from './../../_services/index';

@Component({
    template: `<router-outlet></router-outlet>`
})
export class MyPainelComponent implements OnInit {


    constructor() {
    }

    ngOnInit() {
    }
}
