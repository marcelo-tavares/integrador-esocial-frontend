import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, NavigationStart } from '@angular/router';

import {
    DOCUMENT,
    DomSanitizer,
    SafeHtml
} from '@angular/platform-browser';

import {
    MyAuthService,
    MyHttpService,
    MyAlertService,
    MyOverlayService,
    MyScopeService,
    MyLocalStorageService
} from './../../../_services/index';

import {
    NgbModal,
    ModalDismissReasons,
    NgbModalOptions,
    NgbModalRef
} from '@ng-bootstrap/ng-bootstrap';

import * as moment from 'moment';
import 'moment/locale/pt-br';



import {
    MyDatePipe,
    MyXmlPipe,
    MySearchFilterPipe
} from './../../../_pipes/index';

import { MyBackendRoutes } from './../../../_config/index';
import { MyESocialReturnCodes } from './../../../_config/index';

import * as _ from 'underscore';

@Component({
    templateUrl: './my-painel-detalhe.component.html',
    styleUrls: ['./my-painel-detalhe.component.css']
})
export class MyPainelDetalheComponent implements OnInit {

    eventos: any[];

    idEvento: string;
    descEvento: string;

    eventoDestacadoIndex = -1;

    // Paginação
    pagina: number;
    porPagina: number;
    totalEventos: number;
    eventosPaginaAtual: number;

    zeroEventos = true;

    eventosPagina = [];
    eventoRemover: string;
    titleTodos: string;

    eSocialReturnCodes = MyESocialReturnCodes;

    errosOcorrencias = [];
    xmlEvento: string;

    modalGrande: NgbModalOptions = {
        size: 'lg'
    };

    modalPequeno: NgbModalOptions = {
        size: 'sm'
    };

    modalXMLRef: NgbModalRef;
    modalErrosRef: NgbModalRef;
    modalRemoverRef: NgbModalRef;

    @ViewChild('dlgErros')
    dlgErrosEventos: ElementRef;

    @ViewChild('dlgXML')
    dlgXMLEventos: ElementRef;
    language = 'markup';

    @ViewChild('dlgRemover')
    dlgRemoverEventos: ElementRef;

    @ViewChild('dlgEnviar')
    dlgEnviarEventos: ElementRef;

    mensagemEnvio: string;

    // Filtros
    public isCollapsed = false;
    tooltipFiltro: string;
    situacoesSelecionadas: any = [];
    public disabled = false;
    public apenasComErro = [{ id: 300, text: 'Apenas eventos com Erro' }];

    public layoutsESocial: Array<any> = [];
    public situacoes: Array<any> = [];

    tiposEventoDescricao = [];
    desabilitarBotaoRemover = false;

    constructor(
        private _route: ActivatedRoute,
        private _myOverlayService: MyOverlayService,
        private _myAlertService: MyAlertService,
        private _myHttpService: MyHttpService,
        private _modalService: NgbModal,
        private _myScopeService: MyScopeService,
        private _myLocalStorageService: MyLocalStorageService,
        private _router: Router,
        @Inject(DOCUMENT) private document: Document) {

        this.pagina = 1;
        this.porPagina = 10;
        this.totalEventos = this.porPagina;
        this.carregaLayouts();
        this.carregaSituacoes();
        this.mudaTooltip();
    }

    ngOnInit() {
        this.idEvento = this._route.snapshot.paramMap.get('id');
        this.getEventos(this.pagina, this.porPagina);
    }

    carregaLayouts() {
        const path = MyBackendRoutes.LAYOUTS +  '?sort=id';
        this._myHttpService.get(path)
            .subscribe((response: any) => {
                if (response.status === 200) {
                    this.populaLayouts(response.body._embedded);
                    this.setDescricaoEventos();
                } else {
                    this._myAlertService.error('Erro ao carregar filtro de Eventos');
                }
            });
    }

    populaLayouts(layoutsArr) {
        this.layoutsESocial = [];
        layoutsArr.forEach(layout => {
            const idLayout = +layout.id.slice(2);
            const obj = {
                id: idLayout,
                text: layout.id,
                descricao: layout.descricao
            };
            this.layoutsESocial.push(obj);
        });
    }

    carregaSituacoes() {
        const path = MyBackendRoutes.SITUACOES +  '?sort=id';
        this._myHttpService.get(path)
            .subscribe((response: any) => {
                if (response.status === 200) {
                    this.populaSituacoes(response.body._embedded);
                } else {
                    this._myAlertService.error('Erro ao carregar filtro de Eventos');
                }
            });
        }

    populaSituacoes(situacoesArr) {
        this.situacoes = [];
        situacoesArr.forEach(situacao => {
            const idSituacao = situacao.cdResposta;
            const obj = {
                id: idSituacao,
                text: situacao.descResposta
            };
            this.situacoes.push(obj);
        });
    }

    setDescricaoEventos() {
        this.layoutsESocial.forEach(layout => {
            this.tiposEventoDescricao[layout.text] = layout.descricao;
        });
    }

    mudaTooltip() {
        if (this.isCollapsed) {
            this.tooltipFiltro = 'Expandir Filtros';
        } else {
            this.tooltipFiltro = 'Recolher Filtros';
        }
    }

    toggleFiltro() {
        this.isCollapsed = !this.isCollapsed;
        this.mudaTooltip();
    }


    public atualizaSituacoesSelecionadas(value: any): void {
        this.situacoesSelecionadas = value;
    }

    todosComErro(): boolean {
        for (let i = 0; i < this.situacoesSelecionadas.length; i++) {
            if (this.situacoesSelecionadas[i].id === 300) {
                this.atualizaSituacoesSelecionadas([{ id: 300, text: 'Somente eventos com erro' }]);
                return true;
            }
        }
        return false;
    }

    montarFiltros(IsPesquisaTextual, filter) {

        let   filterStr =
                '&filter={"$and":[' +
                '{"status.cdResposta": {"$gte":' + MyESocialReturnCodes.SUCESSO + '}},' +
                '{"tipo.id":{"$eq":"' + this.idEvento + '"}}';

        if ( IsPesquisaTextual ) {
            filterStr = filterStr + ',{ "xml": { $regex: /' + filter + '/, $options: "sim"}}';
        }

        let filterSituacoesStr = '';

        // Verificar se "Somente eventos com erro" (300) foi selecionado
        if (this.todosComErro()) {
            filterSituacoesStr = ',{"status.cdResposta":{"$gt":300}}';
            this.atualizaSituacoesSelecionadas(this.situacoesSelecionadas);
        } else {
            if (this.situacoesSelecionadas.length) {
                let situacoesStr = '';
                this.situacoesSelecionadas.forEach(situacao => {
                    situacoesStr = situacoesStr + ',' + situacao.id;
                });
                filterSituacoesStr = ',{"status.cdResposta":{"$in":[' + situacoesStr.slice(1) + ']}}';
            }
        }

        filterStr = filterStr + filterSituacoesStr;

        filterStr = filterStr + ']}';

        return filterStr;
    }

    getEventos(page: number = 1, pagesize: number = 10, IsPesquisaTextual: boolean = false , filter: string = '') {
        if (this.totalEventos < pagesize) {
            page = this.pagina = 1;
        }

        const paginacaoStr = '?page=' + page + '&pagesize=' + pagesize;
        const optionsStr = '&hal=f&count';
        const filterStr = this.montarFiltros(IsPesquisaTextual, filter);

        const path = this._myLocalStorageService.getBaseUrl() + MyBackendRoutes.EVENTOS_PAINEL_CONTROLE_DETALHES + paginacaoStr + filterStr + optionsStr;
        this._myOverlayService.mostrar();
        this._myAlertService.clear();
        this._myHttpService.get(path)
            .subscribe((response: any) => {
                if (response.status === 200) {
                    if (response.body._size > 0) {
                        this.zeroEventos = false;
                        this.eventos = response.body._embedded['rh:doc'].map(evt => ({selecionado: false, ...evt}));
                        this.totalEventos = response.body._size;
                        this.eventosPaginaAtual = response.body._returned;
                        this.populaEventosPagina(this.eventos);
                        this.descEvento = this.tiposEventoDescricao[this.idEvento];
                        this.eventoDestacadoIndex = -1;
                    } else {
                        this.zeroEventos = true;
                        this.totalEventos = 0;
                        this.eventos = null;
                        this._myAlertService.warning('Atenção: Nenhum evento foi encontrado para os critérios informados.');
                    }
                } else {
                    this.zeroEventos = true;
                    this.totalEventos = 0;
                    this.eventos = null;
                    this._myAlertService.warning('Atenção: Nenhum evento foi encontrado para os critérios informados.');
                }
                this._myOverlayService.ocultar();
            });
    }

    populaEventosPagina(eventos) {
        this.eventosPagina = [];
        eventos.forEach(evento => {
            this.eventosPagina.push(evento._id.$oid);
        });
    }

    classeSituacao(situacao) {
        let classe = 'text-dark';
        if (situacao > 300) {
            classe = 'text-danger';
        } else if ((situacao === 201) || (situacao === 202)) {
            classe = 'text-success';
        }
        return classe;
    }

    classeIconeSituacao(situacao) {
        let classes: string;
        switch (situacao) {
            case MyESocialReturnCodes.AGUARDANDO_PROCESSAMENTO_ESOCIAL:
                classes = 'fa fa-upload mr-2';
                break;
            case MyESocialReturnCodes.EM_ENVIO_PARA_ESOCIAL:
                classes = 'fa fa-clock-o mr-2';
                break;
            case MyESocialReturnCodes.IMPORTADO:
                classes = 'fa fa-download mr-2';
                break;
            case MyESocialReturnCodes.SUCESSO:
            case MyESocialReturnCodes.SUCESSO_ADVERTENCIA:
                classes = 'fa fa-check mr-2';
                break;
            default:
                if (situacao > 300) {
                    classes = 'fa fa-exclamation-triangle mr-2';
                } else {
                    classes = '';
                }
        }
        return classes;
    }

    vigencia(value: any, momento) {
        let myVigencia: string;
        if (!value) {
            myVigencia = '-';
        } else {
            if (momento === 'inicio') {
                if (!value.iniValid) {
                    myVigencia = '-';
                } else {
                    myVigencia = value.iniValid;
                }
            } else {
                if (!value.fimValid) {
                    myVigencia = '-';
                } else {
                    myVigencia = value.fimValid;
                }
            }
        }
        return myVigencia;
    }

    getInicioVigencia(evento) {
        if (evento.tipo.id === 'S-2230') { // Afastamento
            return evento.chave.dtIniAfast ? moment(evento.chave.dtIniAfast).format('L') : '-';
        } else {
            return evento.vigencia ? evento.vigencia.iniValid ? evento.vigencia.iniValid : '-' : '-';
        }
    }

    getFimVigencia(evento) {
        if (evento.tipo.id === 'S-2230') { // Afastamento
            return evento.chave.dtTermAfast ? moment(evento.chave.dtTermAfast).format('L') : '-';
        } else {
            return evento.vigencia ? evento.vigencia.fimValid ? evento.vigencia.fimValid : '-' : '-';
        }
    }

    getCpfTrab(evento) {
        if (evento.cpfTrab) {
            return evento.cpfTrab;
        } else if (evento.chave.cpfTrab) {
            return evento.chave.cpfTrab;
        } else if (evento.chave.cpfBenef) {
            return evento.chave.cpfBenef;
        } else {
            return null;
        }
    }

    exclusaoEsocial(evento) {
        if ((evento.status.cdResposta === 200 || evento.status.cdResposta === 201 || evento.status.cdResposta === 202)
            && (evento.excluidoEvento === 0)) {
            return true;
        } else {
            return false;
        }
    }


    styleErro(situacao) {
        if (situacao > 300) {
            return 'block';
        } else {
            return 'none';
        }
    }

    refresh(filtrotexto) {
        this._myAlertService.clear();
        this.getEventos(this.pagina, this.porPagina, filtrotexto && filtrotexto !== '', filtrotexto);
    }

    mudaPagina(novapagina, filtrotexto) {
        this.deselecionaTodaPagina();
        this.getEventos(novapagina, this.porPagina, filtrotexto && filtrotexto !== '', filtrotexto);
    }

    onChangePorPagina(porPagina, filtrotexto) {
        this.pagina = 1;
        this.getEventos(this.pagina, this.porPagina, filtrotexto && filtrotexto !== '', filtrotexto);
    }

    backToPainel() {
        this._router.navigate(this._route.snapshot.parent.url);
    }


    montarFiltrosRemover() {
        let filterEventosStr = '';

        let filterStr =
            '*?filter=';

        const eventosSelecionados = this.eventos.filter(evt => evt.selecionado);

        if (eventosSelecionados.length) {
            let eventosStr = '';
            eventosSelecionados.forEach(evento => {
                eventosStr = eventosStr + ',{_id:{$oid:"' + evento + '"}}';
            });
            filterEventosStr = '{$or:[' + eventosStr.slice(1) + ']}';
        }

        filterStr = filterStr + filterEventosStr;

        return filterStr;
    }

    botaoExcluirHabilitar(){
        return this.desabilitarBotaoRemover;
    }

    removerEventos() {
        this.desabilitarBotaoRemover=true;
        const path = this._myLocalStorageService.getBaseUrl() + MyBackendRoutes.EVENTOS + this.montarFiltrosRemover();
        let msgRetorno = ' eventos removidos com sucesso!';
        this._myHttpService.delete(path)
            .subscribe(
            (response: any) => {
                if ((response.status === 200) || (response.status === 204)) {
                    this.verificaPaginaAtual(response.body.deleted);
                    this.getEventos(this.pagina, this.porPagina);
                    if (response.body.deleted === 1) {
                        msgRetorno = ' evento removido com sucesso!';
                    }
                    this._myAlertService.success(response.body.deleted + msgRetorno);
                } else {
                    this._myAlertService.error('Erro ao remover evento(s)');
                }
                this.desabilitarBotaoRemover=false;
                this.modalRemoverRef.close();
            },
            error => {
                let msg = '';
                if (error.status === 404) {
                    msg = '. Registro(s) inexistente';
                }
                this._myAlertService.error('Erro ao remover evento(s)' + msg);
                this.modalRemoverRef.close();
            }
            );
    }

    verificaPaginaAtual(removidos) {
        this.eventosPaginaAtual -= removidos;
        if (this.eventosPaginaAtual === 0) {
            if (this.pagina > 1) {
                this.pagina -= 1;
            }
        }
    }
    enviarEventos() {
        const eventosSelecionados = this.eventos.filter(evt => evt.selecionado);
        if (eventosSelecionados.length > 0) {
            const path = this._myLocalStorageService.getBaseUrl() + MyBackendRoutes.EXCLUSAO_ESOCIAL;
            const payload = [];
            eventosSelecionados.forEach(evento => {
                payload.push({
                    xml: evento.xml,
                    chave: evento.chave,
                    operacao: {
                        id: 3,
                        descricao: 'Exclusão'
                    },
                    tipo: {
                        id: evento.tipo.id,
                        descricao: evento.tipo.descricao
                    },
                    grupo: {
                        id: evento.grupo.id,
                        descricao: evento.grupo.descricao
                    },
                    status: {
                        cdResposta: 99,
                        descResposta: 'Importação pendente'
                    },
                    eventoOriginal: {
                        $oid: evento._id.$oid
                    }
                });
            });
            // adicionado para propositos de log, para saber quais ID's estao sendo excluidos
            // isso será removido no futuro em virtude do log direto na API
            let exclusaoEsocialParam = `?exclusao_esocial=${payload.map(e => e.eventoOriginal.$oid).toString()}`; 

            this._myHttpService.post(path + exclusaoEsocialParam, payload)
                .subscribe((response: any) => {
                    if (response.status === 200) {
                        this.getEventos(this.pagina, this.porPagina);
                    } else {
                        console.log('Erro ao enviar evento');
                    }
                });
        }

    }

    limparSelecionados() {
        this.eventos.forEach(evt => evt.selecionado = false);
    }

    selecionaTodos() {
        this.eventos.forEach(evt => evt.selecionado = true);
    }

    abrirEnviar() {
        const eventosSelecionados = this.eventos.filter(evt => evt.selecionado);
        if (eventosSelecionados.length) {
            if (eventosSelecionados.length === 1) {
                this.mensagemEnvio = '1 evento selecionado para exclusão no eSocial.';
            } else {
                this.mensagemEnvio = eventosSelecionados.length + ' eventos selecionados para exclusão no eSocial.';
            }

            this.enviarEventos();
            this._myAlertService.clear();
            this._modalService.open(this.dlgEnviarEventos, this.modalGrande)
                .result
                .then(
                (result) => {
                    this.limparSelecionados();
                },
                (reason) => {
                    this.limparSelecionados();
                }
                );
        } else {
            this.document.scrollingElement.scrollTop = 0;
            this._myAlertService.error('Ops! Nenhum evento está selecionado para ser excluído do eSocial.');
        }
    }
    // Modals
    abrirErros(ocorrencias) {
        this._myAlertService.clear();

        if (ocorrencias === undefined) {
            this._myAlertService.error('Ops! Ocorrências de erro não encontradas!');
        } else {
            if (!Array.isArray(ocorrencias)) {
                this.errosOcorrencias = [];
                this.errosOcorrencias.push(ocorrencias);
            } else {
                this.errosOcorrencias = ocorrencias;
            }
            this.modalErrosRef = this._modalService.open(this.dlgErrosEventos, this.modalGrande);
        }
    }

    abrirXml(xml) {
        this.xmlEvento = xml;
        this.modalXMLRef = this._modalService.open(this.dlgXMLEventos, this.modalGrande);
    }

    abrirRemover() {
        this.modalRemoverRef = this._modalService.open(this.dlgRemoverEventos, this.modalGrande);
    }

    // Filtros

    filtrarEventos(pagesize: number = 10) {
        this.pagina = 1;
        this.getEventos(this.pagina, pagesize);
    }

    // situacao
    public situacaoSelecionada(value: any): void {
        this.filtrarEventos(this.porPagina);
        // console.log('Selected value is: ', value);
    }

    public situacaoRemovida(value: any): void {
        this.filtrarEventos(this.porPagina);
        // console.log('Removed value is: ', value);
    }

    public atualizaSituacoesSelecionados(value: any): void {
        this.situacoesSelecionadas = value;
    }

    public itemsToString(value: Array<any> = []): string {
        return value
            .map((item: any) => {
                return item.id;
            }).join(',');
    }

    // Checkbox

    togglePagina(ckbEvent) {
        if (ckbEvent.target.checked) {
            this.selecionaTodaPagina();
        } else {
            this.deselecionaTodaPagina();
        }
        // console.log('eventosIds: ', this.eventosSelecionados);
    }

    selecionaTodaPagina() {
        this.selecionaTodos();
    }

    deselecionaTodaPagina() {
        this.limparSelecionados();
    }
    todaPaginaSelecionada(): boolean {
        // const tudoSelecionado = (_.intersection(this.eventosPagina, this.eventosSelecionados).length === this.eventosPagina.length);
        const tudoSelecionado = this.eventos.reduce((a, b) => a.selecionado && b.selecionado);
        if (tudoSelecionado) {
            this.titleTodos = 'Desselecionar todos os eventos';
        } else {
            this.titleTodos = 'Selecionar todos os eventos';
        }
        return tudoSelecionado;
    }

    selecionaEvento(idx: number) {
        this.eventoDestacadoIndex = idx;
    }

}
