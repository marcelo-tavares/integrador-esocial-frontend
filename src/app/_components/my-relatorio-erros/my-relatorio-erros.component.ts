import { Component, OnInit } from '@angular/core';
import {MyEventsService} from "../../_services/index";
import {MyAlertService} from "../../_services/my-alert.service";
import{MyFormatErrorMensageHelper} from './../../_helpers/index'

@Component({
  selector: 'app-my-relatorio-erros',
  templateUrl: './my-relatorio-erros.component.html',
  styleUrls: ['./my-relatorio-erros.component.css']
})
export class MyRelatorioErrosComponent implements OnInit {

    erros: any;
    tiposEvento: any;

    constructor(
        private _myEventsService: MyEventsService,
        private _myAlertService: MyAlertService,
        private _myFormatErroMensageHelper: MyFormatErrorMensageHelper,
    ) { }

    ngOnInit() {
        this.carregaErros();
    }

    carregaErros() {
        this._myEventsService.getRelatorioErros()
            .subscribe((response: any) => {
                if (response.status === 200) {
                    if (response.body._embedded.length > 0) {
                        this.erros = response.body._embedded.sort( (a, b) => (a._id.tipo > b._id.tipo ? 1 : -1));
                            for (var i = 0; i < this.erros.length; i++) { 
                                this._myFormatErroMensageHelper.formataMensagemErro(this.erros[i]._id);
                            }
                        this.carregaTiposEvento();
                    } else {
                        this.erros = [];
                    }
                } else {
                    this._myAlertService.error('Erro ao carregar os Erros');
                }
            });
    }

    carregaTiposEvento() {
        this._myEventsService.getTiposEvento()
            .subscribe((response: any) => {
                if (response.status === 200) {
                    this.tiposEvento = response.body._embedded.map(tipoEvento => ({ id: tipoEvento.id, text: `${tipoEvento.descricao}` }));
                    this.erros = this.erros.map(erro => ({descricaoTipo: this.tiposEvento.find(tipoEvt => erro._id.tipo === tipoEvt.id).text, ...erro}));
                } else {
                    this._myAlertService.error('Erro ao carregar os Tipos de Eventos');
                }
            });
    }

}
