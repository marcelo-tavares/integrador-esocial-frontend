// start:ng42.barrel
export * from './my-config/my-config.component';
export * from './my-content/my-content.component';
export * from './my-dashboard/my-dashboard.component';
export * from './my-login/my-login.component';
export * from './my-painel/my-painel.component';
export * from './my-painel/my-painel-detalhe/my-painel-detalhe.component';
export * from './my-painel/my-painel-list/my-painel-list.component';
export * from './my-pendentes/my-pendentes.component';
export * from './my-eventos-trabalhador/my-eventos-trabalhador.component';
export * from './my-envio-eventos/my-envio-eventos.component';
export * from './my-relatorios/my-relatorios.component';
export * from './my-folha-pagamento/my-folha-pagamento.component';
export * from './my-folha-pagamento/my-agrupamento-folha/my-agrupamento-folha.component';
export * from './my-folha-pagamento/my-detalhe-pagamento/my-detalhe-pagamento.component';
export * from './my-folha-pagamento/my-apuracao-consolidada-imposto-renda/my-apuracao-consolidada-imposto-renda.component';
export * from './my-folha-pagamento/my-apuracao-consolidada-fundo-de-garantia/my-apuracao-consolidada-fundo-de-garantia.component';
export * from './my-folha-pagamento/my-apuracao-consolidada-contribuicoes-sociais/my-apuracao-consolidada-contribuicoes-sociais.component';
export * from './my-qualificacao/my-qualificacao.component';
export * from './my-relatorio-erros/my-relatorio-erros.component';
// end:ng42.barrel

