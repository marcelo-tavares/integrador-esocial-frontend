import {Component, OnDestroy, OnInit, ViewChild, ElementRef} from '@angular/core';

import { MyFolhaPagamento, MyEnumSituacaoFolha, MySituacaoFolha } from './my-folha-pagamento.class';
import { Router } from '@angular/router';
import { MyLocalStorageService, MyHttpService, MyFolhasService} from '../../_services/index';

import { MyBackendRoutes } from './../../_config/index';

import * as moment from 'moment';
import 'moment/locale/pt-br';
import {MyOverlayService} from "../../_services/my-overlay.service";

import { NgbModal, NgbModalRef, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';

import * as XLSX from 'xlsx';
import {MyAlertService} from "../../_services/my-alert.service";



@Component({
    // tslint:disable-next-line:component-selector
    selector: 'app-my-folha-pagamento',
    templateUrl: './my-folha-pagamento.component.html',
    styleUrls: ['./my-folha-pagamento.component.css']
})
export class MyFolhaPagamentoComponent implements OnInit, OnDestroy {
    
    public phoneMask = ['(', /[1-9]/, /\d/, ')', ' ', /\d/ , /\d/ , /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
    public dateMask = [/[0-3]/,/[0-9]/,'/',/[0-1]/,/[1-2]/,'/',/[1-2]/,/[0|9]/,/[0-9]/,/[0-9]/];
    public rgMask = [ /\d/ , /\d/ , /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/];
    public cpfMask = [ /\d/ , /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/ , /\d/, /\d/, '-', /\d/, /\d/];
    public cnpjMask = [ /\d/ , /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/ , /\d/, /\d/, '/', /\d/, /\d/,/\d/, /\d/, '-', /\d/, /\d/];
    public cepMask = [/\d/ , /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/];

    folhaSelecionadaIndex = -1;
    totalFolhas = 0;

    remuneracao = true;
    pagamento = true;
    prodRural = false;
    comProducao = false;
    trabNaoPortuario = false;
    infComplementar = false;
    pagina = 1;
    porPagina = 12;
    totalPagamentos = this.porPagina;
    percentual = 1;
    hideAlertMessage = false;
    
    //dados prestador de serviços
    cnpj = "";
    cpf = "";
    nis = "";
    nome = "";
    dtNasc = "";
    dm = []; // 4 itens
    ctgt = []; // 4 itens
    cnpjest = []; // 4 itens
    locacao = []; // 4itens
    vlrRub = []; // 8 itens    
    cbo = []; //4 itens
    
    folhasPagamento = [];
    folhaSelecionadaObjeto: MyFolhaPagamento;
    situacoesFolha = [];

    // Feature desativada
    apuracaoIndividual = false;

    interval: any = null;

    @ViewChild('dlgFecharFolha')
    dlgFecharFolha: ElementRef;
    modalFecharFolhaRef: NgbModalRef;

    @ViewChild('dlgReabrirFolha')
    dlgReabrirFolha: ElementRef;
    modalReabrirFolhaRef: NgbModalRef;

    @ViewChild('dlgInfoComplementar')
    dlgInfoComplementar: ElementRef;
    modalInfoComplementarRef: NgbModalRef;

    @ViewChild('dlgIncAutonomo')
    dlgIncAutonomo: ElementRef;
    modalIncAutonomoRef: NgbModalRef;

    @ViewChild('dlgImportarFolha')
    dlgImportarFolha: ElementRef;
    modalImportarFolhaRef: NgbModalRef;

    @ViewChild('dlgTransmitirFolha')
    dlgTransmitirFolha: ElementRef;
    modalTransmitirFolhaRef: NgbModalRef;

    modalPequeno: NgbModalOptions = {
        size: 'sm'
    };

    modalGrande: NgbModalOptions = {
        size: 'lg'
    };

    fomatCurrency(evento){
        evento = (evento) ? evento : window.event;
        var charCode = (evento.which) ? evento.which : evento.keyCode;

        var keyboardKeys = (charCode >= 48 && charCode <= 57);

        var numpadKeys = (charCode >= 96 && charCode <= 105);

        if (!(numpadKeys || keyboardKeys)) {
            evento.target.value = evento.target.value.substr(0,evento.target.value.length-1);
        }

        var tempValue = evento.target.value;
        var tm = tempValue.length;
        tempValue = tempValue.replace(/,/g,'');
        
        while(tempValue.substr(0,1) == '0')
            tempValue = tempValue.substr(1, tm);

    
        while(tempValue.length < 3)
            tempValue = '0' + tempValue;

        tempValue = tempValue.substr(0,tempValue.length-2) + ',' + tempValue.substr(tempValue.length-2, tempValue.length)
        evento.target.value = tempValue;
    }

    constructor(
        private router: Router,
        private folhasService: MyFolhasService,
        private _myOverlayService: MyOverlayService,
        private _myAlertService: MyAlertService,
        private _myLocalStorageService: MyLocalStorageService,
        private _modalService: NgbModal,
        private _myHttpService: MyHttpService
    ) { }

    ngOnInit() {
        this._myOverlayService.mostrar();
        this.carregaSituacoesFolha();
        this.carregaFolhas();
        this.interval = setInterval(() => this.carregaFolhas(), 60000);
        this.percentual = 0;
        this.hideAlertMessage = false;
        this.cnpj = "";
        this.cpf = "";
        this.nis = "";
        this.nome = "";
        this.dtNasc = "";
        this.dm = ["","","",""]; // 4 itens
        this.ctgt = ["","","",""]; // 4 itens
        this.cnpjest = ["","","",""]; // 4 itens
        this.locacao = ["","","",""]; // 4 itens
        this.vlrRub = ['0,00','0,00','0,00','0,00','0,00','0,00','0,00','0,00']; // 8 itens    
        this.cbo = ["","","",""]; //4 itens
    }

    ngOnDestroy() {
        if (this.interval) {
            clearInterval(this.interval);
        }
    }

    mudaPagina(novapagina) {
        this.pagina = novapagina;
        this.carregaFolhas();
    }


    carregaFolhas() {
        this.folhasService.getFolhas(this.pagina, this.porPagina)
            .subscribe((response: any) => {
                if (response.status === 200) {

                    if (response.body._size > 0) {
                        this.folhasPagamento = response.body._embedded['rh:doc'].map(folha => (new MyFolhaPagamento(folha)));
                        this.totalFolhas = response.body._size;
                        this.totalPagamentos = this.totalFolhas;
                    } else {
                        this.folhasPagamento = [];
                        this.totalFolhas = 0;
                    }
                }
                this._myOverlayService.ocultar();
            },
            error => {
                this._myOverlayService.ocultar();
                this._myAlertService.error('Erro ao carregar Folhas');
                console.log(error);
            });
    }

    carregaSituacoesFolha() {
        this.folhasService.getSituacoesFolha()
            .subscribe((response: any) => {
                this.situacoesFolha = response.body._embedded.map(situacao => new MySituacaoFolha(situacao));
            });
    }

    selecionaFolha(idx: number) {
        if (idx === this.folhaSelecionadaIndex) {
            this.folhaSelecionadaIndex = -1;
            this.folhaSelecionadaObjeto = null
        } else {
            this.folhaSelecionadaIndex = idx;
            this.folhaSelecionadaObjeto = this.folhasPagamento[this.folhaSelecionadaIndex];
        }
        
    }

    folhaSelecionada(): boolean {
        return this.folhasPagamento.length > 0 && this.folhaSelecionadaIndex >= 0;
    }

    permiteImportar() {
        return this.folhaSelecionada() && this.folhasPagamento[this.folhaSelecionadaIndex].permiteImportar;
    }

    permiteDetalhar() {
        return this.folhaSelecionada() && this.folhasPagamento[this.folhaSelecionadaIndex].permiteDetalhar;
    }

    permiteTransmitir() {
        return this.folhaSelecionada() && this.folhasPagamento[this.folhaSelecionadaIndex].permiteTransmitir;
    }

    permiteImprimirErros() {
        return this.folhaSelecionada() && this.folhasPagamento[this.folhaSelecionadaIndex].permiteImprimirErros;
    }

    permiteTotalizar() {
        return this.folhaSelecionada() && this.folhasPagamento[this.folhaSelecionadaIndex].permiteTotalizar;
    }

    permiteFechar() {
        return this.folhaSelecionada() && this.folhasPagamento[this.folhaSelecionadaIndex].permiteFechar;
    }

    permiteReabrir() {
        return this.folhaSelecionada() && this.folhasPagamento[this.folhaSelecionadaIndex].permiteReabrir;
    }

    apuracaoIndividualContribuicoesSociais(idFolha: string) {
        // TODO: Implementar
        console.log('Funcionalidade não implementada');
    }

    apuracaoIndividualImpostoRenda(idFolha: string) {
        // TODO: Implementar
        console.log('Funcionalidade não implementada');
    }


    apuracaoConsolidadaContribuicoesSociais(idFolha: string) {
        this.router.navigate(['apuracao-consolidada-contribuicoes-sociais', idFolha]);
    }

    apuracaoConsolidadaImpostoRenda(idFolha: string) {
        this.router.navigate(['apuracao-consolidada-imposto-renda', idFolha]);
    }

    apuracaoConsolidadaFundoDeGarantia(idFolha: string) {
        this.router.navigate(['apuracao-consolidada-fundo-de-garantia', idFolha]);
    }

    alteraSituacaoFolha(novaSituacao: MyEnumSituacaoFolha) {
        this.folhasService.atualizaSituacaoFolha(this.folhaSelecionadaObjeto,
                this.situacoesFolha.find(situacao => situacao._id === novaSituacao))
            .subscribe((response: any) => {
                this.carregaFolhas();
            });
    }

    importarEventos() {
        this.alteraSituacaoFolha(MyEnumSituacaoFolha.IMPORTANDO);
    }

    detalharEventos() {
        //agrupamento-folha-pagamento
        //this.router.navigate(['detalhe-folha-pagamento', this.folhasPagamento[this.folhaSelecionadaIndex]._id]);
        this.router.navigate(['agrupamento-folha-pagamento', this.folhasPagamento[this.folhaSelecionadaIndex]._id]);
    }

    transmitirEventos() {
        this.folhasService.transmiteEventosFolha(this.folhasPagamento[this.folhaSelecionadaIndex])
            .subscribe((response: any) => {
                console.log('Eventos transmitidos');
            });
        this.alteraSituacaoFolha(MyEnumSituacaoFolha.TRANSMITINDO);
    }

    imprimirErros() {
        const folha = this.folhasPagamento[this.folhaSelecionadaIndex]._id;
        let errosFolha = [];
        this.folhasService.getErrosFolha(folha)
            .subscribe((response: any) => {
                if (response.status === 200) {
                    const errosFolhaPagina = response.body._embedded.map(erroFolha => ([
                        erroFolha.tipo_erro,
                        erroFolha.tipo,
                        moment(erroFolha.dhGeracao.$date).format('L') + ' ' + moment(erroFolha.dhGeracao.$date).format('LTS'),
                        erroFolha.cpfTrab,
                        erroFolha.descricao,
                        erroFolha.localizacao
                    ]));
                    errosFolha = [...errosFolha, ...errosFolhaPagina];

                    const totalPaginas = response.body._total_pages;
                    if (totalPaginas === 1) {
                        this.downloadErrosFolha(errosFolha);
                    } else {
                        for (var pagina = 2; pagina <= totalPaginas; pagina++) {
                            this.folhasService.getErrosFolha(folha, pagina)
                                .subscribe((response: any) => {
                                    if (response.status === 200) {
                                        const errosFolhaPagina = response.body._embedded.map(erroFolha => ([
                                            erroFolha.tipo_erro,
                                            erroFolha.tipo,
                                            moment(erroFolha.dhGeracao.$date).format('L') + ' ' + moment(erroFolha.dhGeracao.$date).format('LTS'),
                                            erroFolha.cpfTrab,
                                            erroFolha.descricao,
                                            erroFolha.localizacao
                                        ]));
                                        errosFolha = [...errosFolha, ...errosFolhaPagina];

                                        if (errosFolha.length === response.body._size) {
                                            this.downloadErrosFolha(errosFolha);
                                        }
                                    }
                                })
                        }
                    }
                } else {
                    console.log('Erro ao carregar os erros da folha');
                }
            });
    }

    private downloadErrosFolha(errosFolha: any[]) {
        errosFolha.unshift(['Tipo', 'Evento', 'Data/Hora Geração', 'CPF', 'Descrição', 'Localização']);
        const worksheet = XLSX.utils.aoa_to_sheet(errosFolha);
        var wscols = [
            {wch:10},
            {wch:10},
            {wch:20},
            {wch:20},
            {wch:120},
            {wch:60}
        ];
        worksheet['!cols'] = wscols;
        const workbook: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(workbook, worksheet, 'Erros');

        XLSX.writeFile(workbook, 'Erros-Folha-' + this.folhasPagamento[this.folhaSelecionadaIndex]._id + '.xlsx');
    }

    totalizarEventos() {
        this.alteraSituacaoFolha(MyEnumSituacaoFolha.TOTALIZANDO);
    }

    fecharFolha() {
        this.alteraSituacaoFolha(MyEnumSituacaoFolha.FECHANDO);
    }

    reabrirFolha() {
        this.alteraSituacaoFolha(MyEnumSituacaoFolha.REABRINDO);
    }

    classeSituacao(situacao) {
        let classe: string;
        switch (situacao) {
            case MyEnumSituacaoFolha.IMPORTADA:
            case MyEnumSituacaoFolha.TRANSMITIDA:
            case MyEnumSituacaoFolha.FECHADA:
                classe = 'text-success';
                break;
            case MyEnumSituacaoFolha.TRANSMITIDA_COM_ERRO:
            case MyEnumSituacaoFolha.REABERTA_COM_ERRO:
            case MyEnumSituacaoFolha.FECHADA_COM_ERRO:
                classe = 'text-danger';
                break;
            default:
                classe = 'text-dark';
        }
        return classe;
    }

    classeIconeSituacao(situacao) {
        let classes: string;
        switch (situacao) {
            case MyEnumSituacaoFolha.REABRINDO:
            case MyEnumSituacaoFolha.FECHANDO:
            case MyEnumSituacaoFolha.IMPORTANDO:
            case MyEnumSituacaoFolha.TOTALIZANDO:
            case MyEnumSituacaoFolha.TRANSMITINDO:
                classes = 'fa fa-spinner mr-2';
                break;
            case MyEnumSituacaoFolha.IMPORTADA:
                classes = 'fa fa-download mr-2';
                break;
            case MyEnumSituacaoFolha.TRANSMITIDA:
            case MyEnumSituacaoFolha.TRANSMITIDA_COM_ERRO:
                classes = 'fa fa-upload mr-2';
                break;
            case MyEnumSituacaoFolha.FECHADA:
            case MyEnumSituacaoFolha.FECHADA_COM_ERRO:
                classes = 'fa fa-lock mr-2';
                break;
            case MyEnumSituacaoFolha.ABERTA:
            case MyEnumSituacaoFolha.REABERTA:
            case MyEnumSituacaoFolha.REABERTA_COM_ERRO:
                classes = 'fa fa-unlock mr-2';
                break;
            default:
                classes = '';
        }
        return classes;
    }


    xmlEvento1200(nrInsc,periodo){
        var xml = '<eSocial xmlns="http://www.esocial.gov.br/schema/evt/evtInfoComplPer/v02_05_00">' + "\n";
        xml += "    <evtRemun>\n";
        xml += "        <ideEvento>\n";
        xml += "            <indRetif>1</indRetif>\n";
        xml += "            <indApuracao>1</indApuracao>\n";
        xml += "            <perApur>" + periodo + "</perApur>\n";
		xml += "        	    <tpAmb>1</tpAmb>\n";
        xml += "            <procEmi>1</procEmi>\n";
        xml += "            <verProc>02.04.02</verProc>\n";
        xml += "        </ideEvento>\n";
        xml += "        <ideEmpregador>\n";
        xml += "            <tpInsc>1</tpInsc>\n";
        xml += "            <nrInsc>" + nrInsc + "</nrInsc>\n";
        xml += "        </ideEmpregador>\n";

        xml += "        <ideTrabalhador>\n";
        xml += "            <cpfTrab>" + this.cpf + "</cpfTrab>\n";
        xml += "            <nisTrab>" + this.nis + "</nisTrab>\n";
        xml += "            <infoComplem>\n";
        xml += "                <nmTrab>" + this.nome + "</nmTrab>\n";
        xml += "                <dtNascto>" + this.dtNasc + "</dtNascto>\n";
        xml += "            </infoComplem>\n";
        xml += "        </ideTrabalhador>\n";

        for(var x = 0; x < this.dm.length;x++){
            if(this.dm[x] !==''){
                xml += "        <dmDev>\n";
                xml += "        <ideDmDev>" + this.dm[x] + "</ideDmDev>\n";
                xml += "        <codCateg>" +  this.ctgt[x] + "</codCateg>\n";
                xml += "        <infoPerApur>\n";
                xml += "            <ideEstabLot>\n";
                xml += "                <tpInsc>1</tpInsc>\n";
                xml += "                <nrInsc>" + this.cnpjest[x] + "</nrInsc>\n";
                xml += "                <codLotacao>" + this.locacao[x] + "</codLotacao>\n";
                xml += "                <remunPerApur>\n";

                if(this.vlrRub[x] !=='0,00') {
                    xml += "                    <itensRemun>\n";
                    xml += "                        <codRubr>05-0512</codRubr>\n";
                    xml += "                        <ideTabRubr>UNICA</ideTabRubr>\n";
                    xml += "                        <vrRubr>" + this.vlrRub[x] + "</vrRubr>\n";
                    xml += "                    </itensRemun>\n";
                }
                if(this.vlrRub[x+4] !=='0,00') {
                    xml += "                    <itensRemun>\n";
                    xml += "                        <codRubr>01-9000</codRubr>\n";
                    xml += "                        <ideTabRubr>UNICA</ideTabRubr>\n";
                    xml += "                        <vrRubr>" + this.vlrRub[x+4] + "</vrRubr>\n";
                    xml += "                    </itensRemun>\n";
                }

                xml += "                </remunPerApur>\n";
                xml += "            </ideEstabLot>\n";
                xml += "        </infoPerApur>\n";
                xml += "        <infoComplCont>\n";
                xml += "            <codCBO>" + this.cbo[x] + "</codCBO>\n";
                xml += "        </infoComplCont>\n";
                xml += "        </dmDev>\n";
            }
        }
        xml += "    </evtRemun>\n";
        xml += "</eSocial>";

        return xml;
    }

    inclusaoiAutonomo(){
        //periodo
        var periodo = this.folhasPagamento[this.folhaSelecionadaIndex]._id;

               
        //nrInsc
        var nrInsc = atob(this._myLocalStorageService.get('usrcnum'));


        this.enviarEvento1200(this.xmlEvento1200(nrInsc.substr(0,8),periodo));
    }

    enviarEvento1200(evento) {
        const path = this._myLocalStorageService.getBaseUrl() + MyBackendRoutes.ARQUIVO_EVENTOS;
        const payload = [];
        payload.push({xml: evento});
        //xml
        this._myHttpService.post(path, payload)
            .subscribe((response: any) => {
                if (response.status === 200) {
                    //envio realizado com sucesso
                    this.hideAlertMessage = true;
                    setTimeout( () => {
                        this.hideAlertMessage = false;
                    }, 2000);
                } else {
                    console.log('Erro ao enviar evento');
                }
            });
    }

     xmlEvento1280(indice,periodo, nrInsc, percentual){
        var xml = '<eSocial xmlns="http://www.esocial.gov.br/schema/evt/evtInfoComplPer/v02_05_00">';
        xml += "\n    <evtInfoComplPer>";
        xml += "\n        <ideEvento>";
        xml += "\n            <indRetif>1</indRetif>";
        xml += "\n            <indApuracao>" + indice + "</indApuracao>";
        xml += "\n            <perApur>" + periodo + "</perApur>";
        xml += "\n            <tpAmb>1</tpAmb>";
        xml += "\n            <procEmi>1</procEmi>";
        xml += "\n            <verProc>02.05.00</verProc>";
        xml += "\n        </ideEvento>";
        xml += "\n        <ideEmpregador>";
        xml += "\n            <tpInsc>1</tpInsc>";
        xml += "\n            <nrInsc>" + nrInsc+ "</nrInsc>";
        xml += "\n        </ideEmpregador>";
        xml += "\n        <infoSubstPatr>";
        xml += "\n            <indSubstPatr>2</indSubstPatr>";
        xml += "\n            <percRedContrib>" + percentual + "</percRedContrib>";
        xml += "\n        </infoSubstPatr>";
        xml += "\n    </evtInfoComplPer>";
        xml += "\n</eSocial>";

        return xml;
     }

    percentualFolha(){
        //periodo
        var periodo = this.folhasPagamento[this.folhaSelecionadaIndex]._id;

        //indice
        var indice = 1;
        if(periodo.substr(5,2) === '13')indice = 2;
        
        //nrInsc
        var nrInsc = atob(this._myLocalStorageService.get('usrcnum'));

        this.enviarEvento(this.xmlEvento1280(indice,periodo, nrInsc.substr(0,8), this.percentual));
    }


    enviarEvento(evento) {
        const path = this._myLocalStorageService.getBaseUrl() + MyBackendRoutes.ARQUIVO_EVENTOS;
        const payload = [];
        payload.push({xml: evento});
        //xml
        this._myHttpService.post(path, payload)
            .subscribe((response: any) => {
                if (response.status === 200) {
                    //envio realizado com sucesso
                    this.hideAlertMessage = true;
                    setTimeout( () => {
                        this.hideAlertMessage = false;
                    }, 2000);
                } else {
                    console.log('Erro ao enviar evento');
                }
            });
    }


    abreDlgFecharFolha() {
        this.modalFecharFolhaRef = this._modalService.open(this.dlgFecharFolha, this.modalPequeno);
    }

    abreDlgReabrirFolha() {
        this.modalReabrirFolhaRef = this._modalService.open(this.dlgReabrirFolha, this.modalPequeno);
    }

    abreDlgInfoImplementares() {
        this.modalInfoComplementarRef = this._modalService.open(this.dlgInfoComplementar, this.modalGrande);
    }

    abreDlgIncAutonomos(){
        
        this.modalIncAutonomoRef = this._modalService.open(this.dlgIncAutonomo, this.modalGrande);
    }

    abreDlgImportarFolha() {
        this.modalImportarFolhaRef = this._modalService.open(this.dlgImportarFolha, this.modalPequeno);
    }

    abreDlgTransmitirFolha() {
        this.modalTransmitirFolhaRef = this._modalService.open(this.dlgTransmitirFolha, this.modalPequeno);
    }
}
