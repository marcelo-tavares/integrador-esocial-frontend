import { Component, OnInit } from '@angular/core';
import { parseString } from 'xml2js';
import { MyFolhasService } from '../../../_services/my-folhas.service';
import { ActivatedRoute } from '@angular/router';
import { MyAlertService } from "../../../_services/my-alert.service";
import { Location } from '@angular/common';
import { MyOverlayService } from "../../../_services/my-overlay.service";

@Component({
    selector: 'app-my-apuracao-consolidada-contribuicoes-sociais',
    templateUrl: './my-apuracao-consolidada-contribuicoes-sociais.component.html',
    styleUrls: ['./my-apuracao-consolidada-contribuicoes-sociais.component.css']
})
export class MyApuracaoConsolidadaContribuicoesSociaisComponent implements OnInit {

    dados: any = null;
    dadosDesc: any = null;

    constructor(
        private _myFolhasService: MyFolhasService,
        private _route: ActivatedRoute,
        private _myAlertService: MyAlertService,
        private _location: Location,
        private _myOverlayService: MyOverlayService
    ) { }

    ngOnInit() {
        this.carregaFolha();
    }

    getInfoContribDesc(infoCS){
        this._myFolhasService.getInfoContrib(infoCS.infoContrib.classTrib)
        .subscribe((response: any) => {
            if (response.status === 200) {
                if (response.body._returned > 0) {

                    this.dadosDesc = response.body;
                    
                    //Recria o objeto infoContrib adicionando Descrição referente ao tipo
                    Object.defineProperty(infoCS.infoContrib,'descricao', {value : this.dadosDesc._embedded[0].descricao,
                        writable : true,
                        enumerable : true,
                        configurable : true});
                }

            } else {
                this._myAlertService.error('Erro ao carregar Folha de Pagamento');
            }
        });
    }

    getCategTrab(basesRemun){
        this._myFolhasService.getCategTrab(basesRemun.codCateg)
        .subscribe((response: any) => {
            if (response.status === 200) {
                if (response.body._returned > 0) {
                    this.dadosDesc = response.body;
                    //Recria o objeto basesRemun adicionando Descrição referente ao tipo
                    Object.defineProperty(basesRemun,'descricao', {value : this.dadosDesc._embedded[0].descricao,
                    writable : true,
                    enumerable : true,
                    configurable : true});
                }

            } else {
                this._myAlertService.error('Erro ao carregar Folha de Pagamento');
            }
        });
    }

    carregaFolha() {
        this._myOverlayService.mostrar();
        this._myFolhasService.getFolhasTotalizadores(this._route.snapshot.params['idFolha'])
            .subscribe((response: any) => {
                if (response.status === 200) {
                    
                    if (response.body._size > 0) {

                        this.dados = response.body.totalizador_5011;

                        //dados.infoCS.infoContrib.classTrib
                        this.getInfoContribDesc(this.dados.infoCS)

                        // Converte objetos que deveriam ser listas
                        if (!Array.isArray(this.dados.infoCS.ideEstab)) {
                            this.dados.infoCS.ideEstab = this.dados.infoCS.ideEstab ? [this.dados.infoCS.ideEstab] : [];
                        }
                        for (let ideEstab of this.dados.infoCS.ideEstab) {
                            if (!Array.isArray(ideEstab.ideLotacao)) {
                                ideEstab.ideLotacao = ideEstab.ideLotacao ? [ideEstab.ideLotacao] : [];
                            }

                            for (let ideLotacao of ideEstab.ideLotacao) {
                                if (!Array.isArray(ideLotacao.infoTercSusp)) {
                                    ideLotacao.infoTercSusp = ideLotacao.infoTercSusp ? [ideLotacao.infoTercSusp] : [];
                                }
                                if (!Array.isArray(ideLotacao.basesRemun)) {
                                    ideLotacao.basesRemun = ideLotacao.basesRemun ? [ideLotacao.basesRemun] : [];
                                    //Buscar Categoria do Trabalhador
                                    for (let basesRenum of ideLotacao.basesRemun) {
                                        this.getCategTrab(basesRenum);
                                    }
                                }
                                if (!Array.isArray(ideLotacao.infoSubstPatrOpPort)) {
                                    ideLotacao.infoSubstPatrOpPort = ideLotacao.infoSubstPatrOpPort ? [ideLotacao.infoSubstPatrOpPort] : [];
                                }
                            }

                            if (!Array.isArray(ideEstab.basesAquis)) {
                                ideEstab.basesAquis = ideEstab.basesAquis ? [ideEstab.basesAquis] : [];
                            }

                            if (!Array.isArray(ideEstab.basesComerc)) {
                                ideEstab.basesComerc = ideEstab.basesComerc ? [ideEstab.basesComerc] : [];
                            }

                            if (!Array.isArray(ideEstab.infoCREstab)) {
                                ideEstab.infoCREstab = ideEstab.infoCREstab ? [ideEstab.infoCREstab] : [];
                            }
                        }
                        if (!Array.isArray(this.dados.infoCS.infoCRContrib)) {
                            this.dados.infoCS.infoCRContrib = this.dados.infoCS.infoCRContrib ? [this.dados.infoCS.infoCRContrib] : [];
                        }
                    }

                } else {
                    this._myAlertService.error('Erro ao carregar Folha de Pagamento');
                }
                this._myOverlayService.ocultar();
            });
    }

    voltar() {
        this._location.back();
    }

    imprimir() {
        const printContent = document.getElementById("conteudoImpressao");
        const WindowPrt = window.open('', '', 'left=0,top=0,width=900,height=900,toolbar=0,scrollbars=0,status=0');
        WindowPrt.document.write(printContent.innerHTML);
        WindowPrt.document.close();
        WindowPrt.focus();
        WindowPrt.print();
        WindowPrt.close();
        //window.print();
    }
}
