export enum MyEnumSituacaoFolha {
    ABERTA = 97,
    REABERTA = 98,
    IMPORTANDO = 99,
    IMPORTADA = 100,
    TRANSMITINDO = 101,
    TRANSMITIDA_COM_ERRO = 102,
    TRANSMITIDA = 103,
    TOTALIZANDO = 104,
    TOTALIZADA = 105,
    FECHANDO = 106,
    FECHADA = 107,
    REABRINDO = 108,
    FECHADA_COM_ERRO = 109,
    REABERTA_COM_ERRO = 110
}

export class MySituacaoFolha {
    _id: MyEnumSituacaoFolha;
    descricao: string;

    constructor(json: any) {
        this._id = json._id;
        this.descricao = json.descricao;
    }
}

export class MyFolhaPagamento {
    _id: number;
    status: MySituacaoFolha;
    remuneracao: boolean;
    pagamento: boolean;
    prodRural: boolean;
    comProducao: boolean;
    trabNaoPortuario: boolean;
    infComplementar: boolean;

    constructor(json: any) {
        this._id = json._id;
        this.status = new MySituacaoFolha(json.status);
        this.remuneracao = json.remuneracao;
        this.pagamento = json.pagamento;
        this.prodRural = json.prodRural;
        this.comProducao = json.comProducao;
        this.trabNaoPortuario = json.trabNaoPortuario;
        this.infComplementar = json.infComplementar;

    }

    private get emTransicao(): boolean {
        return this.status._id === MyEnumSituacaoFolha.IMPORTANDO
            || this.status._id === MyEnumSituacaoFolha.TRANSMITINDO
            || this.status._id === MyEnumSituacaoFolha.TOTALIZANDO
            || this.status._id === MyEnumSituacaoFolha.FECHANDO
            || this.status._id === MyEnumSituacaoFolha.REABRINDO;
    }

    get permiteApuracaoIndividual(): boolean {
        return !this.emTransicao && (this.status._id === MyEnumSituacaoFolha.TRANSMITIDA || this.status._id === MyEnumSituacaoFolha.FECHADA);
    }

    get permiteApuracaoConsolidada(): boolean {
        return !this.emTransicao && this.status._id === MyEnumSituacaoFolha.FECHADA;
        //return this.totalizador_5011 || this.totalizador_5012;
    }

    get permiteImportar(): boolean {
        return !this.emTransicao && this.status._id !== MyEnumSituacaoFolha.FECHADA;
    }

    get permiteTransmitir(): boolean {
        return !this.emTransicao && (this.status._id === MyEnumSituacaoFolha.IMPORTADA
            || this.status._id === MyEnumSituacaoFolha.TRANSMITIDA_COM_ERRO);
    }

    get permiteImprimirErros(): boolean {
        return !this.emTransicao && (this.status._id === MyEnumSituacaoFolha.TRANSMITIDA_COM_ERRO
            || this.status._id === MyEnumSituacaoFolha.FECHADA_COM_ERRO);
    }

    get permiteTotalizar(): boolean {
        return !this.emTransicao && (this.status._id === MyEnumSituacaoFolha.TRANSMITIDA);
    }

    get permiteFechar(): boolean {
        return !this.emTransicao && (this.status._id === MyEnumSituacaoFolha.TRANSMITIDA
            || this.status._id === MyEnumSituacaoFolha.TRANSMITIDA_COM_ERRO
            || this.status._id === MyEnumSituacaoFolha.TOTALIZADA
            || this.status._id === MyEnumSituacaoFolha.FECHADA_COM_ERRO
            || this.status._id === MyEnumSituacaoFolha.REABERTA
            || this.status._id === MyEnumSituacaoFolha.REABERTA_COM_ERRO);
    }

    get permiteReabrir(): boolean {
        return !this.emTransicao && (this.status._id === MyEnumSituacaoFolha.FECHADA
            || this.status._id === MyEnumSituacaoFolha.REABERTA_COM_ERRO);
    }

    get permiteDetalhar(): boolean {
        return !this.emTransicao;
    }
}
