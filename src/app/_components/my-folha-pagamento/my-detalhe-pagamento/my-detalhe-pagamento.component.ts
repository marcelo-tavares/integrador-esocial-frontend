import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MyESocialReturnCodes } from '../../../_config';
import { MyEventFilter, MyEventsService, MyAlertService, MyFolhasService } from '../../../_services';
import { NgbModal, NgbModalRef, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { Location } from '@angular/common';

import { ActivatedRoute } from '@angular/router';
import {MyEnumSituacaoFolha, MyFolhaPagamento} from '../my-folha-pagamento.class';
import {MyOverlayService} from "../../../_services/my-overlay.service";

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'app-my-detalhe-pagamento',
    templateUrl: './my-detalhe-pagamento.component.html',
    styleUrls: ['./my-detalhe-pagamento.component.css']
})
export class MyDetalhePagamentoComponent implements OnInit {

    folha: MyFolhaPagamento = null;

    eventoDestacadoIndex = -1;

    tiposEvento: any[];
    situacoes: any[];
    operacoes: any[];

    desabilitarBotaoRemover = false;

    filtros: MyEventFilter = new MyEventFilter();

    paginaSelecionada = false;

    eventos: any[] = [];
    totalEventos: number;
    eventosPorPagina = 10;
    pagina = 1;

    @ViewChild('dlgXML')
    dlgXMLEventos: ElementRef;
    modalEnviarRef: NgbModalRef;
    modalXMLRef: NgbModalRef;
    xmlEvento: string;
    language = 'markup';

    @ViewChild('dlgErros')
    dlgErrosEventos: ElementRef;
    errosOcorrencias = [];
    modalErrosRef: NgbModalRef;

    @ViewChild('dlgRemover')
    dlgRemoverEventos: ElementRef;
    modalRemoverRef: NgbModalRef;

    @ViewChild('dlgRemoverESocial')
    dlgRemoverEventosESocial: ElementRef;
    modalRemoverESocialRef: NgbModalRef;

    modalGrande: NgbModalOptions = {
        size: 'lg'
    };
    modalPequeno: NgbModalOptions = {
        size: 'sm'
    };

    constructor(
        private _myAlertService: MyAlertService,
        private _myEventsService: MyEventsService,
        private _myFolhasService: MyFolhasService,
        private _modalService: NgbModal,
        private _route: ActivatedRoute,
        private _location: Location,
        private _myOverlayService: MyOverlayService
    ) { }

    ngOnInit() {
        this.filtros.folha = this._route.snapshot.params['idFolha'];
        this.filtros.tipos = [{id:this._route.snapshot.params['idEvento'], text:this._route.snapshot.params['idEvento']}];
        this.carregaFolha();
        this.carregaTiposEvento();
        this.carregaSituacoes();
        this.carregaOperacoes();
        this.carregaEventos();
    }

    carregaFolha() {
        this._myFolhasService.getFolha(this._route.snapshot.params['idFolha'])
            .subscribe((response: any) => {
                if (response.status === 200) {
                    this.folha = new MyFolhaPagamento(response.body);
                } else {
                    this._myAlertService.error('Erro ao carregar Folha de Pagamento');
                }
            });
    }

    carregaTiposEvento() {
        this._myEventsService.getTiposEvento()
            .subscribe((response: any) => {
                if (response.status === 200) {
                    this.tiposEvento = response.body._embedded.map(tipoEvento => ({ id: tipoEvento.id, text: `${tipoEvento.id} - ${tipoEvento.descricao}` }));
                    this.carregaDescricaoEvento(this._route.snapshot.params['idEvento']);
                } else {
                    this._myAlertService.error('Erro ao carregar os Tipos de Eventos');
                }
            });
    }

    carregaDescricaoEvento(idEvento){
        for(var x = 0; x < this.tiposEvento.length;x++){
            if(this.tiposEvento[x].id === idEvento) {
                this.filtros.tipos = [{id:idEvento, text:this.tiposEvento[x].text}];
            }
        }
    }

    dump(obj) {
        var out = '';
        for (var i in obj) {
            if(typeof obj[i] == "object")
                this.dump(obj[i]);
            else
                out += i + ": " + obj[i] + "\n";
        }
    
        console.log(out);
    }

    atualizaTiposEventoSelecionados(tipos: any[]) {
        this.filtros.tipos = tipos;
        this.carregaEventos(1, this.eventosPorPagina);
    }

    carregaSituacoes() {
        this._myEventsService.getSituacoesEvento()
            .subscribe((response: any) => {
                if (response.status === 200) {
                    this.situacoes = response.body._embedded.map(situacao => ({ id: situacao.cdResposta, text: situacao.descResposta }));
                    // A consulta original não traz as situações de sucesso
                    this.situacoes.push({id: MyESocialReturnCodes.SUCESSO, text: 'Sucesso'});
                    this.situacoes.push({id: MyESocialReturnCodes.SUCESSO_ADVERTENCIA, text: 'Sucesso com advertência'});
                } else {
                    this._myAlertService.error('Erro ao carregar Situações dos Eventos');
                }
            });
    }

    atualizaSituacoesSelecionadas(situacoes: any[]) {
        this.filtros.statuses = situacoes;
        this.carregaEventos(1, this.eventosPorPagina);
    }

    carregaOperacoes() {
        this._myEventsService.getOperacoesEvento()
            .subscribe((response: any) => {
                if (response.status === 200) {
                    this.operacoes = response.body._embedded.map(operacao => ({ id: operacao.id, text: operacao.descricao }));
                } else {
                    this._myAlertService.error('Erro ao carregar Operações dos Eventos');
                }
            });
    }

    atualizaOperacoesSelecionadas(operacoes: any[]) {
        this.filtros.operacoes = operacoes;
        this.carregaEventos(1, this.eventosPorPagina);
    }

    carregaEventos(page: number = 1, pagesize: number = 10) {
        this._myOverlayService.mostrar();
        this.pagina = page;
        this._myEventsService.getEventosFolhaPagamento(this.filtros, page, pagesize)
            .subscribe((response: any) => {
                if (response.status === 200) {
                    if (response.body._size > 0) {
                        this.eventos = response.body._embedded['rh:doc'].map(evt => ({selecionado: false, ...evt}));
                        this.totalEventos = response.body._size;
                        this.eventoDestacadoIndex = -1;
                    } else {
                        this.eventos = [];
                        this.totalEventos = 0;
                    }
                } else {
                    this._myAlertService.error('Erro ao carregar os Eventos');
                }
                this._myOverlayService.ocultar();
            },
            error => {
                this._myOverlayService.ocultar();
                this._myAlertService.error('Erro ao carregar os Eventos');
                console.log(error);
            });
    }

    mudaPagina(novapagina) {
        this.carregaEventos(novapagina, this.eventosPorPagina);
    }

    onChangePorPagina(porPagina) {
        this.pagina = 1;
        this.carregaEventos(this.pagina, this.eventosPorPagina);
    }

    removerEventos() {
        this._myEventsService.removeEventos(this.eventos.filter(evt => evt.selecionado && this.podeExcluirLocal(evt)))
            .subscribe(
                (response: any) => {
                    if ((response.status === 200) || (response.status === 204)) {
                        this.carregaEventos(this.pagina, this.eventosPorPagina);
                        this._myAlertService.success(response.body.deleted + ' evento(s) removido(s) com sucesso!');
                    } else {
                        this._myAlertService.error('Erro ao remover evento(s)');
                    }
                    this.modalRemoverRef.close();
                },
                error => {
                    let msg = '';
                    if (error.status === 404) {
                        msg = '. Registro(s) inexistente';
                    }
                    this._myAlertService.error('Erro ao remover evento(s)' + msg);
                    this.modalRemoverRef.close();
                }
            );
    }

    botaoExcluirHabilitar(){
        return this.desabilitarBotaoRemover;
    }

    removerEventosESocial() {
        this.desabilitarBotaoRemover=true;
        this._myEventsService.excluirEventosESocial(this.eventos.filter(evt => evt.selecionado && this.podeExcluirDoESocial(evt)))
            .subscribe(
                (response: any) => {
                    if ((response.status === 200) || (response.status === 204)) {
                        this.carregaEventos(this.pagina, this.eventosPorPagina);
                        this._myAlertService.success(response.body.inserted + ' evento(s) removido(s) com sucesso!');
                    } else {
                        this._myAlertService.error('Erro ao remover evento(s)');
                    }
                    this.desabilitarBotaoRemover=false;
                    this.modalRemoverESocialRef.close();
                },
                error => {
                    let msg = '';
                    if (error.status === 404) {
                        msg = '. Registro(s) inexistente';
                    }
                    this._myAlertService.error('Erro ao remover evento(s)' + msg);
                    this.modalRemoverESocialRef.close();
                }
            );
    }

    toggleSelecionaPagina() {
        this.eventos.filter(evt => this.podeExcluir(evt)).forEach(evt => evt.selecionado = this.paginaSelecionada);
    }

    podeExcluir(evento: any) {
        return this.podeExcluirLocal(evento) || this.podeExcluirDoESocial(evento);
    }

    podeExcluirLocal(evento: any) {
        // Pode excluir somente eventos que não possuem recibo do e-social
        return !evento.nrRecibo;
    }

    todosPodeExcluirLocal() {
        const eventosSelecionados = this.eventos.filter(evt => evt.selecionado);
        return eventosSelecionados.length > 0 && eventosSelecionados.every(evt => this.podeExcluirLocal(evt));
    }

    podeExcluirDoESocial(evento: any) {
        return (evento.nrRecibo && (evento.excluidoEvento === 0 || evento.excluidoEvento === undefined));
    }

    todosPodeExcluirESocial() {
        const eventosSelecionados = this.eventos.filter(evt => evt.selecionado);
        return eventosSelecionados.length > 0 && eventosSelecionados.every(evt => this.podeExcluirDoESocial(evt));
    }

    abrirXml(xml) {
        this.xmlEvento = xml;
        this.modalXMLRef = this._modalService.open(this.dlgXMLEventos, this.modalGrande);
    }

    abrirErros(ocorrencias) {
        this._myAlertService.clear();

        if (ocorrencias === undefined) {
            this._myAlertService.error('Ops! Ocorrências de erro não encontradas!');
        } else {
            if (!Array.isArray(ocorrencias)) {
                this.errosOcorrencias = [];
                this.errosOcorrencias.push(ocorrencias);
            } else {
                this.errosOcorrencias = ocorrencias;
            }
            this.modalErrosRef = this._modalService.open(this.dlgErrosEventos, this.modalGrande);
        }
    }

    confirmaRemoverLocal() {
        if (this.eventos.filter(evt => evt.selecionado === true).length > 0) {
            this.modalRemoverRef = this._modalService.open(this.dlgRemoverEventos, this.modalPequeno);
        }
    }

    confirmaRemoverESocial() {
        if (this.eventos.filter(evt => evt.selecionado === true).length > 0) {
            this.modalRemoverESocialRef = this._modalService.open(this.dlgRemoverEventosESocial, this.modalPequeno);
        }
    }

    voltar() {
        this._location.back();
    }

    classeSituacao(situacao) {
        let classe = 'text-dark';
        if (situacao > 300) {
            classe = 'text-danger';
        } else if ((situacao === 100) || (situacao === 201) || (situacao === 202)) {
            classe = 'text-success';
        }
        return classe;
    }

    classeIconeSituacao(situacao) {
        let classes: string;
        switch (situacao) {
            case MyESocialReturnCodes.AGUARDANDO_PROCESSAMENTO_ESOCIAL:
                classes = 'fa fa-upload mr-2';
                break;
            case MyESocialReturnCodes.EM_ENVIO_PARA_ESOCIAL:
                classes = 'fa fa-clock-o mr-2';
                break;
            case MyESocialReturnCodes.IMPORTADO:
                classes = 'fa fa-download mr-2';
                break;
            case MyESocialReturnCodes.SUCESSO:
            case MyESocialReturnCodes.SUCESSO_ADVERTENCIA:
                classes = 'fa fa-check mr-2';
                break;
            default:
                if (situacao > 300) {
                    classes = 'fa fa-exclamation-triangle mr-2';
                } else {
                    classes = '';
                }
        }
        return classes;
    }

    codigoErro(codigo) {
        let retorno = '';
        if (codigo > 300) {
            retorno = '(' + codigo.toString() + ') ';
        }
        return retorno;
    }

    selecionaEvento(idx: number) {
        this.eventoDestacadoIndex = idx;
    }
}
