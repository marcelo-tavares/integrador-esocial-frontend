import { Component, OnInit } from '@angular/core';
import { parseString } from 'xml2js';
import { MyFolhasService } from "../../../_services/my-folhas.service";
import { ActivatedRoute } from "@angular/router";
import { MyAlertService } from "../../../_services/my-alert.service";
import { Location } from '@angular/common';
import { MyOverlayService } from "../../../_services/my-overlay.service";

@Component({
    selector: 'app-my-apuracao-consolidada-imposto-renda',
    templateUrl: './my-apuracao-consolidada-imposto-renda.component.html',
    styleUrls: ['./my-apuracao-consolidada-imposto-renda.component.css']
})
export class MyApuracaoConsolidadaImpostoRendaComponent implements OnInit {

    dados: any = null;

    constructor(
        private _myFolhasService: MyFolhasService,
        private _route: ActivatedRoute,
        private _location: Location,
        private _myAlertService: MyAlertService,
        private _myOverlayService: MyOverlayService
    ) { }

    ngOnInit() {
        this.carregaFolha();
    }

    carregaFolha() {
        this._myOverlayService.mostrar();
        this._myFolhasService.getFolhasTotalizadores(this._route.snapshot.params['idFolha'])
            .subscribe((response: any) => {
                if (response.status === 200) {
                    
                    if (response.body._size > 0) {

                        this.dados = response.body.totalizador_5012;

                        // Converte objetos que deveriam ser listas
                        if (!Array.isArray(this.dados.infoIRRF.infoCRContrib)) {
                            this.dados.infoIRRF.infoCRContrib = this.dados.infoIRRF.infoCRContrib ? [this.dados.infoIRRF.infoCRContrib] : [];
                        }
                    }

                } else {
                    this._myAlertService.error('Erro ao carregar Folha de Pagamento');
                }

                this._myOverlayService.ocultar();
            });
    }

    voltar() {
        this._location.back();
    }

    imprimir() {
        const printContent = document.getElementById("conteudoImpressao");
        const WindowPrt = window.open('', '', 'left=0,top=0,width=900,height=900,toolbar=0,scrollbars=0,status=0');
        WindowPrt.document.write(printContent.innerHTML);
        WindowPrt.document.close();
        WindowPrt.focus();
        WindowPrt.print();
        WindowPrt.close();
        //window.print();
    }
}
