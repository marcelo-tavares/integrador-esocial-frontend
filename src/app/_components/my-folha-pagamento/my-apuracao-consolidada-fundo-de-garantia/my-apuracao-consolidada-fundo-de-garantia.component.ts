import { Component, OnInit } from '@angular/core';
import { parseString } from 'xml2js';
import { MyFolhasService } from "../../../_services/my-folhas.service";
import { ActivatedRoute } from "@angular/router";
import { MyAlertService } from "../../../_services/my-alert.service";
import { Location, getLocaleExtraDayPeriodRules } from '@angular/common';
import { MyOverlayService } from "../../../_services/my-overlay.service";

@Component({
    selector: 'app-my-apuracao-consolidada-fundo-de-garantia',
    templateUrl: './my-apuracao-consolidada-fundo-de-garantia.component.html',
    styleUrls: ['./my-apuracao-consolidada-fundo-de-garantia.component.css']
})
export class MyApuracaoConsolidadaFundoDeGarantiaComponent implements OnInit {

    dados: any = null;
    dadosDesc: any = null;

    constructor(
        private _myFolhasService: MyFolhasService,
        private _route: ActivatedRoute,
        private _location: Location,
        private _myAlertService: MyAlertService,
        private _myOverlayService: MyOverlayService
    ) { }

    ngOnInit() {
        this.carregaFolha();
    }

    getBaseFGTSDesc(basePerApur, valor){
        this._myFolhasService.getBaseFgts(valor)
        .subscribe((response: any) => {
            if (response.status === 200) {
                if (response.body._returned > 0) {

                    this.dadosDesc = response.body;
                    //Recria o objeto basePerApur adicionando Descição referente ao tipo
                    Object.defineProperty(basePerApur,'descricao', {value : this.dadosDesc._embedded[0].descricao,
                        writable : true,
                        enumerable : true,
                        configurable : true});
                }

            } else {
                this._myAlertService.error('Erro ao carregar Folha de Pagamento');
            }
        });
    }

    getDpsFGTSDesc(infoDpsFGTS){
        this._myFolhasService.getDpsFgts(infoDpsFGTS.tpDps)
        .subscribe((response: any) => {
            if (response.status === 200) {
                if (response.body._returned > 0) {
                    this.dadosDesc = response.body;
                    //Recria o objeto dpsPerApur adicionando Descição referente ao tipo
                    Object.defineProperty(infoDpsFGTS,'descricao', {value : this.dadosDesc._embedded[0].descricao,
                        writable : true,
                        enumerable : true,
                        configurable : true});
                }

            } else {
                this._myAlertService.error('Erro ao carregar Folha de Pagamento');
            }
        });
    }

    carregaFolha() {
        this._myOverlayService.mostrar();
        this._myFolhasService.getFolhasTotalizadores(this._route.snapshot.params['idFolha'])
            .subscribe((response: any) => {
                if (response.status === 200) {
                    
                    if (response.body._size > 0) {
                        this.dados = response.body.totalizador_5013;

                        // Converte objetos que deveriam ser listas
                        if (this.dados){
                            if (!Array.isArray(this.dados.infoFGTS.infoBaseFGTS.basePerApur)) {
                                this.dados.infoFGTS.infoBaseFGTS.basePerApur = this.dados.infoFGTS.infoBaseFGTS.basePerApur ? [this.dados.infoFGTS.infoBaseFGTS.basePerApur] : [];
                            }
                            for (let infoBaseFGTS of this.dados.infoFGTS.infoBaseFGTS) {
                                if (!Array.isArray(infoBaseFGTS.infoBasePerAntE)) {
                                    infoBaseFGTS.infoBasePerAntE = infoBaseFGTS.infoBasePerAntE ? [infoBaseFGTS.infoBasePerAntE] : [];
                                    for (let infoBasePerAntE of infoBaseFGTS.infoBasePerAntE) {
                                        this.getBaseFGTSDesc(infoBasePerAntE.basePerAntE,infoBasePerAntE.basePerAntE.tpValorE);
                                    }
                                }
                            }

                            for (let basePerApurs of this.dados.infoFGTS.infoBaseFGTS.basePerApur) {
                                this.getBaseFGTSDesc(basePerApurs,basePerApurs.tpValor);
                            }

                            if (!Array.isArray(this.dados.infoFGTS.infoDpsFGTS.dpsPerApur)) {
                                this.dados.infoFGTS.infoDpsFGTS.dpsPerApur = this.dados.infoFGTS.infoDpsFGTS.dpsPerApur ? [this.dados.infoFGTS.infoDpsFGTS.dpsPerApur] : [];
                            }
                            for (let dpsPerApur of this.dados.infoFGTS.infoDpsFGTS.dpsPerApur) {
                                this.getDpsFGTSDesc(dpsPerApur);
                            }
                        }
                    }

                } else {
                    this._myAlertService.error('Erro ao carregar Folha de Pagamento');
                }

                this._myOverlayService.ocultar();
            });
    }

    voltar() {
        this._location.back();
    }

    imprimir() {
        const printContent = document.getElementById("conteudoImpressao");
        const WindowPrt = window.open('', '', 'left=0,top=0,width=900,height=900,toolbar=0,scrollbars=0,status=0');
        WindowPrt.document.write(printContent.innerHTML);
        WindowPrt.document.close();
        WindowPrt.focus();
        WindowPrt.print();
        WindowPrt.close();
    }
}
