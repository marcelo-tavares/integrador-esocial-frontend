import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import {
    DOCUMENT
} from '@angular/platform-browser';

import {
    NgbModal,
    NgbModalOptions,
    NgbModalRef
} from '@ng-bootstrap/ng-bootstrap';

import {
    MyAuthService,
    MyAlertService,
    MyHttpService,
    MyOverlayService,
    MyScopeService,
    MyLocalStorageService
} from './../../_services/index';

import{
    MyFormatErrorMensageHelper
} from './../../_helpers/index'


import { MyBackendRoutes } from './../../_config/index';
import { MyESocialReturnCodes } from './../../_config/index';

import * as _ from 'underscore';
import './../../_types/my_types';


@Component({
    templateUrl: './my-pendentes.component.html',
    styleUrls: ['./my-pendentes.component.css'],
})
export class MyPendentesComponent implements OnInit {

    usuarioCorrente: UsuarioCorrente;
    eventos: any[];

    eventoDestacadoIndex = -1;

    // Paginação
    pagina: number;
    porPagina: number;
    totalEventos: number;

    eventosSelecionados = [];
    eventosSelecionadosExcluir = [];
    eventosPagina = [];
    eventosPaginaAtual: number;
    eventoRemover: string;
    titleTodos: string;
    desabilitarBotaoRemover = false;

    eSocialReturnCodes = MyESocialReturnCodes;

    closeResult: string;

    errosOcorrencias = [];
    xmlEvento: string;

    modalGrande: NgbModalOptions = {
        size: 'lg'
    };

    modalPequeno: NgbModalOptions = {
        size: 'sm'
    };

    modalEnviarRef: NgbModalRef;
    modalXMLRef: NgbModalRef;
    modalErrosRef: NgbModalRef;
    modalRemoverRef: NgbModalRef;

    @ViewChild('dlgEnviar')
    dlgEnviarEventos: ElementRef;

    @ViewChild('dlgErros')
    dlgErrosEventos: ElementRef;

    @ViewChild('dlgXML')
    dlgXMLEventos: ElementRef;
    language = 'markup';

    @ViewChild('dlgRemover')
    dlgRemoverEventos: ElementRef;

    isOver = false;
    zeroEventos = true;
    tooltipFiltro: string;
    mensagemEnvio: string;

    // Filtros
    public isCollapsed = false;
    tiposSelecionados: any = [];
    situacoesSelecionadas: any = [];
    gruposSelecionados: any = [];
    public disabled = false;
    public apenasComErro = [{ id: 300, text: 'Apenas eventos com Erro' }];

    // Filtro - Layouts
    public layoutsESocial: Array<any> = [];

    // Filtro - Situacoes
    public situacoes: Array<any> = [];

    // Filtro - Tipos de Evento
    public tiposEvento: Array<any> = [
        { id: 1, text: 'Eventos de Tabelas' },
        { id: 2, text: 'Eventos Não Periódicos' }
    ];

    constructor(
        private _myAuthService: MyAuthService,
        private _myAlertService: MyAlertService,
        private _myHttpService: MyHttpService,
        private _myOverlayService: MyOverlayService,
        private _modalService: NgbModal,
        private _myScopeService: MyScopeService,
        private _myLocalStorageService: MyLocalStorageService,
        private _myFormatErroMensageHelper: MyFormatErrorMensageHelper,
        @Inject(DOCUMENT) private document: Document) {

        this.usuarioCorrente = this._myScopeService.usuarioCorrente;
        this.pagina = 1;
        this.porPagina = 10;
        this.totalEventos = this.porPagina;
        this.mudaTooltip();
        this.carregaLayouts();
        this.carregaSituacoes();
    }

    carregaLayouts() {
        const path = MyBackendRoutes.LAYOUTS +  '?sort=id';
        this._myHttpService.get(path)
            .subscribe((response: any) => {
                if (response.status === 200) {
                    this.populaLayouts(response.body._embedded);
                } else {
                    this._myAlertService.error('Erro ao carregar filtro de Eventos');
                }
            });
    }

    populaLayouts(layoutsArr) {
        this.layoutsESocial = [];
        layoutsArr.forEach(layout => {
            const idLayout = +layout.id.slice(2);
            const obj = {
                id: idLayout,
                text: layout.id
            };
            this.layoutsESocial.push(obj);
        });
    }

    carregaSituacoes() {
        const path = MyBackendRoutes.SITUACOES +  '?sort=id';
        this._myHttpService.get(path)
            .subscribe((response: any) => {
                if (response.status === 200) {
                    this.populaSituacoes(response.body._embedded);
                } else {
                    this._myAlertService.error('Erro ao carregar filtro de Eventos');
                }
            });
        }

    populaSituacoes(situacoesArr) {
        this.situacoes = [];
        situacoesArr.forEach(situacao => {
            const idSituacao = situacao.cdResposta;
            const obj = {
                id: idSituacao,
                text: situacao.descResposta
            };
            this.situacoes.push(obj);
        });
    }

    mudaTooltip() {
        if (this.isCollapsed) {
            this.tooltipFiltro = 'Expandir Filtros';
        } else {
            this.tooltipFiltro = 'Recolher Filtros';
        }
    }

    toggleFiltro() {
        this.isCollapsed = !this.isCollapsed;
        this.mudaTooltip();
    }

    ngOnInit() {
        this.verificaScope();
        this.getEventos(this.pagina, this.porPagina);
    }

    verificaScope() {
        if (this._myScopeService.filtrarImportados) {
            this.situacoesSelecionadas.push({ id: 100, text: 'Importado' });
        } else {
            if (this._myScopeService.filtrarAutorizados) {
                this.situacoesSelecionadas.push({ id: 101, text: 'Envio para o eSocial autorizado' });
            } else {
                if (this._myScopeService.filtrarEnviados) {
                    this.situacoesSelecionadas.push({ id: 102, text: 'Enviado para o eSocial' });
                } else {
                    if (this._myScopeService.filtrarErros) {
                        this.situacoesSelecionadas.push({ id: 300, text: 'Somente eventos com erro' });
                    }
                }
            }
        }
        this._myScopeService.clearFiltrar();
    }

    mudaPagina(novapagina) {
        this.getEventos(novapagina, this.porPagina);
    }

    onChangePorPagina(porPagina) {
        this.pagina = 1;
        this.getEventos(this.pagina, this.porPagina);
    }

    todosComErro(): boolean {
        for (let i = 0; i < this.situacoesSelecionadas.length; i++) {
            if (this.situacoesSelecionadas[i].id === 300) {
                this.atualizaSituacoesSelecionados([{ id: 300, text: 'Somente eventos com erro' }]);
                return true;
            }
        }
        return false;
    }

    montarFiltros(IsPesquisaTextual, filter) {
        let filterStr =
            '&filter={"$and":[' +
            '{"status.cdResposta": {"$ne":' + MyESocialReturnCodes.SUCESSO + '}},' +
            '{"status.cdResposta": {"$ne":' + MyESocialReturnCodes.SUCESSO_ADVERTENCIA + '}},' +
            '{"grupo.id": {"$ne":' + 3 +  '}}';

        if (IsPesquisaTextual) {
            filterStr = filterStr + ',{ "xml": { $regex: /' + filter + '/, $options: "sim"}}';
        }

        let filterTiposStr = '';
        let filterSituacoesStr = '';
        let filterGruposStr = '';

        // Filtrar layouts selecionados
        if (this.tiposSelecionados.length) {
            let tiposStr = '';
            this.tiposSelecionados.forEach(layout => {
                tiposStr = tiposStr + ',"S-' + layout.id + '"';
            });
            filterTiposStr = ',{"tipo.id":{"$in":[' + tiposStr.slice(1) + ']}}';
        }

        // Filtrar situacoes selecionadas
        //

        // Verificar se "Somente eventos com erro" (300) foi selecionado
        if (this.todosComErro()) {
            filterSituacoesStr = ',{"status.cdResposta":{"$gt":300}}';
            this.atualizaSituacoesSelecionados(this.situacoesSelecionadas);
        } else {
            if (this.situacoesSelecionadas.length) {
                let situacoesStr = '';
                this.situacoesSelecionadas.forEach(situacao => {
                    situacoesStr = situacoesStr + ',' + situacao.id;
                });
                filterSituacoesStr = ',{"status.cdResposta":{"$in":[' + situacoesStr.slice(1) + ']}}';
            }
        }

        // Filtrar tipos de eventos selecionados
        if (this.gruposSelecionados.length) {
            let gruposStr = '';
            this.gruposSelecionados.forEach(tipo => {
                gruposStr = gruposStr + ',' + tipo.id;
            });
            filterGruposStr = ',{"grupo.id":{"$in":[' + gruposStr.slice(1) + ']}}';
        }

        filterStr = filterStr + filterTiposStr + filterSituacoesStr + filterGruposStr;

        filterStr = filterStr + ']}';

        // console.log(filterStr);

        return filterStr;
    }


    filtrarEventos(pagesize: number = 10) {
        this.pagina = 1;
        this.getEventos(this.pagina, pagesize);
    }

    getEventos(page: number = 1, pagesize: number = 10, IsPesquisaTextual: boolean = false, filter: string = '') {
        if (this.totalEventos < pagesize) {
            page = this.pagina = 1;
        }
        const paginacaoStr = '?page=' + page + '&pagesize=' + pagesize;
        const sortStr = '&sort={"tipo.id":1, "dhGeracao":-1}';
        const optionsStr = '&hal=f&count';
        const filterStr = this.montarFiltros(IsPesquisaTextual, filter);

        const path = this._myLocalStorageService.getBaseUrl() + MyBackendRoutes.PENDENTES + paginacaoStr + filterStr + optionsStr + sortStr;
        this._myOverlayService.mostrar();
        this._myAlertService.clear();
        this._myHttpService.get(path)
            .subscribe((response: any) => {
                if (response.status === 200) {
                    if (response.body._size > 0) {
                        this.zeroEventos = false;
                        this.eventos = response.body._embedded['rh:doc'];
                        this.totalEventos = response.body._size;
                        this.eventosPaginaAtual = response.body._returned;
                        this.eventosSelecionados = [];
                        this.eventosSelecionadosExcluir = [];
                        this.populaEventosPagina(this.eventos);
                        this.eventoDestacadoIndex = -1;
                    } else {
                        this.zeroEventos = true;
                        this.totalEventos = 0;
                        this.eventos = null;
                        this._myAlertService.warning('Atenção: Nenhum evento foi encontrado para os critérios informados.');
                    }
                } else {
                    this.zeroEventos = true;
                    this.totalEventos = 0;
                    this.eventos = null;
                    this._myAlertService.error('Erro no acesso aos dados.');
                }
                this._myOverlayService.ocultar();
            },
                error => {
                    this.zeroEventos = true;
                    this.totalEventos = 0;
                    this.eventos = null;
                    this._myAlertService.error('Erro no acesso aos dados.');
                    this._myOverlayService.ocultar();
                });
    }

    codigoErro(codigo) {
        let retorno = '';
        if (codigo > 300) {
            retorno = '(' + codigo.toString() + ') ';
        }
        return retorno;
    }

    refresh() {
        this._myAlertService.clear();
        this.getEventos(this.pagina, this.porPagina);
    }

    classeSituacao(situacao) {
        let classe = 'text-dark';
        if (situacao > 300) {
            classe = 'text-danger';
        } else if (situacao === 100) {
            classe = 'text-success';
        }
        return classe;
    }

    classeIconeSituacao(situacao) {
        let classes: string;
        switch (situacao) {
            case MyESocialReturnCodes.AGUARDANDO_PROCESSAMENTO_ESOCIAL:
                classes = 'fa fa-upload mr-2';
                break;
            case MyESocialReturnCodes.EM_ENVIO_PARA_ESOCIAL:
                classes = 'fa fa-clock-o mr-2';
                break;
            case MyESocialReturnCodes.IMPORTADO:
                classes = 'fa fa-download mr-2';
                break;
            default:
                if (situacao > 300) {
                    classes = 'fa fa-exclamation-triangle mr-2';
                } else {
                    classes = '';
                }
        }
        return classes;
    }

    styleErro(situacao) {
        if (situacao > 300) {
            return 'block';
        } else {
            return 'none';
        }
    }

    styleRemover(situacao) {
        if (situacao === MyESocialReturnCodes.AGUARDANDO_PROCESSAMENTO_ESOCIAL) {
            return 'none';
        } else {
            return 'block';
        }
    }

    importado(situacao) {
        if (situacao === this.eSocialReturnCodes.IMPORTADO) {
            return true;
        } else {
            return false;
        }
    }

    comErro(situacao) {
        return situacao > 300;
    }

    processandoNoESocial(situacao) {
        if (situacao === this.eSocialReturnCodes.AGUARDANDO_PROCESSAMENTO_ESOCIAL) {
            return true;
        } else {
            return false;
        }
    }

    envioAutorizado(situacao) {
        if (situacao === this.eSocialReturnCodes.EM_ENVIO_PARA_ESOCIAL) {
            return true;
        } else {
            return false;
        }
    }

    pendente(situacao) {
        if ((situacao !== this.eSocialReturnCodes.SUCESSO) &&
            (situacao !== this.eSocialReturnCodes.SUCESSO_ADVERTENCIA)) {
            return true;
        } else {
            return false;
        }
    }

    atualizaEventosSelecionados(ckbEvent, situacao) {

        // Eventos selecionados para enviar
        if (this.importado(situacao) || this.comErro(situacao)) {
            if (ckbEvent.target.checked) {
                if (this.eventosSelecionados.indexOf(ckbEvent.target.name) < 0) {
                    this.eventosSelecionados.push(ckbEvent.target.name);
                }
            } else {
                if (this.eventosSelecionados.indexOf(ckbEvent.target.name) > -1) {
                    this.eventosSelecionados.splice(this.eventosSelecionados.indexOf(ckbEvent.target.name), 1);
                }
            }
        }

        // Eventos selecionados para excluir
        if (!this.processandoNoESocial(situacao)) {
            if (ckbEvent.target.checked) {
                if (this.eventosSelecionadosExcluir.indexOf(ckbEvent.target.name) < 0) {
                    this.eventosSelecionadosExcluir.push(ckbEvent.target.name);
                }
            } else {
                if (this.eventosSelecionadosExcluir.indexOf(ckbEvent.target.name) > -1) {
                    this.eventosSelecionadosExcluir.splice(this.eventosSelecionadosExcluir.indexOf(ckbEvent.target.name), 1);
                }
            }
        }
    }

    populaEventosPagina(eventos) {
        this.eventosPagina = [];
        eventos.forEach(evento => {
            const obj = {
                oid: evento._id.$oid,
                situacao: evento.status ? evento.status.cdResposta : 0
            };
            this.eventosPagina.push(obj);
        });
    }

    enviarEventos() {
        this.eventosSelecionados.forEach(evento => {
            const path = this._myLocalStorageService.getBaseUrl() + MyBackendRoutes.EVENTOS + evento;
            const payload = {
                status: {
                    cdResposta: 101,
                    descResposta: 'Envio para o eSocial autorizado'
                }
            };
            this._myHttpService.patch(path, payload)
                .subscribe((response: any) => {
                    if (response.status === 200) {
                        this.getEventos(this.pagina, this.porPagina);
                    } else {
                        console.log('Erro ao enviar evento');
                    }
                });
        });
    }

    montarFiltrosRemover() {
        return '*?filter={$or:[{_id:{$oid:"' + this.eventoRemover + '"}}]}';
    }


    removerEvento() {
        const path = this._myLocalStorageService.getBaseUrl() + MyBackendRoutes.EVENTOS + this.montarFiltrosRemover();
        this._myHttpService.delete(path)
            .subscribe(
                (response: any) => {
                    if ((response.status === 200) || (response.status === 204)) {
                        this.verificaPaginaAtual(response.body.deleted);
                        this.getEventos(this.pagina, this.porPagina);
                        this._myAlertService.success('1 evento removido com sucesso!');
                    } else {
                        this._myAlertService.error('Erro ao remover evento');
                    }
                    this.modalRemoverRef.close();
                },
                error => {
                    let msg = '';
                    if (error.status === 404) {
                        msg = '. Registro(s) inexistente';
                    }
                    this._myAlertService.error('Erro ao remover evento(s)' + msg);
                    this.modalRemoverRef.close();
                }
            );
    }

    montarFiltrosRemoverLista() {
        let filterEventosStr = '';

        let filterStr =
            '*?filter=';

        if (this.eventosSelecionadosExcluir.length) {
            let eventosStr = '';
            this.eventosSelecionadosExcluir.forEach(evento => {
                eventosStr = eventosStr + ',{_id:{$oid:"' + evento + '"}}';
            });
            filterEventosStr = '{$or:[' + eventosStr.slice(1) + ']}';
        }

        filterStr = filterStr + filterEventosStr;

        return filterStr;
    }

    botaoExcluirHabilitar(){
        return this.desabilitarBotaoRemover;
    }

    removerEventos() {
        this.desabilitarBotaoRemover=true;
        const path = this._myLocalStorageService.getBaseUrl() + MyBackendRoutes.EVENTOS + this.montarFiltrosRemoverLista();
        let msgRetorno = ' eventos removidos com sucesso!';
        this._myHttpService.delete(path)
            .subscribe(
                (response: any) => {
                    if ((response.status === 200) || (response.status === 204)) {
                        this.verificaPaginaAtual(response.body.deleted);
                        this.getEventos(this.pagina, this.porPagina);
                        this.eventosSelecionadosExcluir = [];
                        if (response.body.deleted === 1) {
                            msgRetorno = ' evento removido com sucesso!';
                        }
                        this.desabilitarBotaoRemover=false;
                        this._myAlertService.success(response.body.deleted + msgRetorno);
                    } else {
                        this._myAlertService.error('Erro ao remover evento(s)');
                    }
                    this.modalRemoverRef.close();
                },
                error => {
                    let msg = '';
                    if (error.status === 404) {
                        msg = '. Registro(s) inexistente';
                    }
                    this._myAlertService.error('Erro ao remover evento(s)' + msg);
                    this.modalRemoverRef.close();
                }
            );
    }

    verificaPaginaAtual(removidos) {
        this.eventosPaginaAtual -= removidos;
        if (this.eventosPaginaAtual === 0) {
            if (this.pagina > 1) {
                this.pagina -= 1;
            }
        }
    }

    retornaAoImportado(evento) {
        const path = this._myLocalStorageService.getBaseUrl() + MyBackendRoutes.EVENTOS + evento;
        const payload = {
            status: {
                cdResposta: 100,
                descResposta: 'Importado'
            }
        };
        this._myHttpService.patch(path, payload)
            .subscribe((response: any) => {
                if (response.status === 200) {
                    this.getEventos(this.pagina, this.porPagina);
                    this.eventosSelecionados = [];
                } else {
                    this._myAlertService.error('Erro ao alterar evento');
                }
            });
    }

    togglePagina(ckbEvent) {
        if (ckbEvent.target.checked) {
            this.selecionaTodaPagina();
        } else {
            this.deselecionaTodaPagina();
        }
    }

    selecionaTodaPagina() {
        this.deselecionaTodaPagina();
        this.eventosPagina.forEach(evento => {

            // selecionar para enviar
            if (this.importado(evento.situacao)) {
                if (this.eventosSelecionados.indexOf(evento.oid) < 0) {
                    this.eventosSelecionados.push(evento.oid);
                }
            }

            // selecionar para excluir
            if (!this.processandoNoESocial(evento.situacao)) {
                if (this.eventosSelecionadosExcluir.indexOf(evento.oid) < 0) {
                    this.eventosSelecionadosExcluir.push(evento.oid);
                }
            }

        });
    }

    deselecionaTodaPagina() {
        this.eventosSelecionados = [];
        this.eventosSelecionadosExcluir = [];
    }

    todaPaginaSelecionada(): boolean {
        const tudoSelecionado = (this.eventosSelecionadosExcluir.length) && (this.eventosSelecionadosExcluir.length === this.eventosPagina.length);
        if (tudoSelecionado) {
            this.titleTodos = 'Desselecionar todos os eventos';
        } else {
            this.titleTodos = 'Selecionar todos os eventos';
        }
        return tudoSelecionado;
    }

    verificaChecked(oid): boolean {
        const enviar = this.eventosSelecionados && (-1 !== this.eventosSelecionados.indexOf(oid));
        const excluir = this.eventosSelecionadosExcluir && (-1 !== this.eventosSelecionadosExcluir.indexOf(oid));

        return (enviar || excluir);
    }

    abrirEnviar() {
        if (this.eventosSelecionados.length) {
            if (this.eventosSelecionados.length === 1) {
                this.mensagemEnvio = '1 evento autorizado para envio.';
            } else {
                this.mensagemEnvio = this.eventosSelecionados.length + ' eventos autorizados para envio.';
            }

            this.enviarEventos();
            this._myAlertService.clear();
            this._modalService.open(this.dlgEnviarEventos, this.modalGrande)
                .result
                .then(
                    (result) => {
                        this.eventosSelecionados = [];
                    },
                    (reason) => {
                        this.eventosSelecionados = [];
                    }
                );
        } else {
            this.document.scrollingElement.scrollTop = 0;
            this._myAlertService.error('Ops! Nenhum evento está selecionado para envio ou os eventos selecionados não podem ser enviados.');
        }
    }

    abrirErros(ocorrencias) {
        this._myAlertService.clear();

        if (ocorrencias === undefined) {
            this._myAlertService.error('Ops! Ocorrências de erro não encontradas!');
        } else {
            if (!Array.isArray(ocorrencias)) {
                this.errosOcorrencias = [];
                this.errosOcorrencias.push(ocorrencias);
            } else {
                this.errosOcorrencias = ocorrencias;
            }

            for (var i = 0; i < this.errosOcorrencias.length; i++) { 
                  this._myFormatErroMensageHelper.formataMensagemErro(this.errosOcorrencias[i]);  
            }

            this.modalErrosRef = this._modalService.open(this.dlgErrosEventos, this.modalGrande);
        }
    }

    abrirXml(xml) {
        this.xmlEvento = xml;
        this.modalXMLRef = this._modalService.open(this.dlgXMLEventos, this.modalGrande);
    }

    abrirRemover(evento) {
        this.eventoRemover = evento;
        this.modalRemoverRef = this._modalService.open(this.dlgRemoverEventos, this.modalGrande);
    }

    abrirRemoverLista() {

        // TODO testar de há eventos selecionados para exclusão

        this.modalRemoverRef = this._modalService.open(this.dlgRemoverEventos, this.modalGrande);
    }

    // Filtros

    // layout
    public layoutSelecionado(value: any): void {
        this.filtrarEventos(this.porPagina);
        // console.log('Selected value is: ', value);
    }

    public layoutRemovido(value: any): void {
        this.filtrarEventos(this.porPagina);
        // console.log('Removed value is: ', value);
    }

    public atualizaLayoutsSelecionados(value: any): void {
        this.tiposSelecionados = value;
    }

    // situacao
    public situacaoSelecionada(value: any): void {
        this.filtrarEventos(this.porPagina);
        // console.log('Selected value is: ', value);
    }

    public situacaoRemovida(value: any): void {
        this.filtrarEventos(this.porPagina);
        // console.log('Removed value is: ', value);
    }

    public atualizaSituacoesSelecionados(value: any): void {
        this.situacoesSelecionadas = value;
    }

    // tipos de evento
    public tipoEventoSelecionado(value: any): void {
        this.filtrarEventos(this.porPagina);
        // console.log('Selected value is: ', value);
    }

    public tipoEventoRemovido(value: any): void {
        this.filtrarEventos(this.porPagina);
        // console.log('Removed value is: ', value);
    }

    public atualizaTiposEventoSelecionados(value: any): void {
        this.gruposSelecionados = value;
    }

    public itemsToString(value: Array<any> = []): string {
        return value
            .map((item: any) => {
                return item.id;
            }).join(',');
    }

    private mostraListas() {
        console.log('eventosSelecionados:' + this.eventosSelecionados);
        console.log('eventosSelecionadosExcluir:' + this.eventosSelecionadosExcluir);
        console.log('----------------------------');
    }

    selecionaEvento(idx: number) {
        this.eventoDestacadoIndex = idx;
    }

}
