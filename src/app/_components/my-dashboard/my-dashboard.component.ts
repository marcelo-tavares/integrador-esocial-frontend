import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import {
    MyAuthService,
    MyHttpService,
    MyAlertService,
    MyOverlayService,
    MyScopeService,
    MyLocalStorageService,
    MyActiveUserService
} from './../../_services/index';

import { MyBackendRoutes } from './../../_config/index';
import { MyESocialReturnCodes } from './../../_config/index';

import './../../_types/my_types';
import './../../_config/my-constantes.config';
import { MyConstantes } from './../../_config/my-constantes.config';

@Component({
    templateUrl: './my-dashboard.component.html',
    styleUrls: ['./my-dashboard.component.css']
})
export class MyDashboardComponent implements OnInit {

    pendentes: number;
    public totaisSituacao: any[] = [];

    constructor(
        private _myHttpService: MyHttpService,
        private _myAlertService: MyAlertService,
        private _myAuthService: MyAuthService,
        private _myOverlayService: MyOverlayService,
        private _myScopeService: MyScopeService,
        private _myLocalStorageService: MyLocalStorageService,
        private _myActiveUserService: MyActiveUserService,
        private _router: Router) {
    }

    ngOnInit() {

        // TODO: Criar recupeação de dados reais para o Dashboard
        this.pendentes = 15;
        //this._myAuthService.checkAuth();

        this.getEventosDashboard();
        this._myActiveUserService.get();
    }

    getEventosDashboard() {
        const optionsStr = '?hal=f&count';

        const path = this._myLocalStorageService.getBaseUrl() + MyBackendRoutes.EVENTOS_POR_STATUS;
        this._myOverlayService.mostrar();
        this._myAlertService.clear();
        this._myHttpService.get(path)
            .subscribe((response: any) => {
                if (response.status === 200) {
                    this.totalizarPorSituacao(response.body._embedded);
                } else {
                    this._myAlertService.warning('Erro ao recuperar dados de totalização (' + response.status + ')');
                }
                this._myOverlayService.ocultar();
            });
    }

    totalizarPorSituacao(array) {
        let grp: number;

        this.totaisSituacao[MyConstantes.IMPORTADOS] = 0;
        this.totaisSituacao[MyConstantes.COM_ENVIO_AUTORIZADO] = 0;
        this.totaisSituacao[MyConstantes.AGUARDANDO_PROCESSAMENTO_ESOCIAL] = 0;
        this.totaisSituacao[MyConstantes.RECEBIDOS_COM_SUCESSO] = 0;
        this.totaisSituacao[MyConstantes.COM_ERRO] = 0;

        array.forEach(element => {
            switch (element._id.status_cdResposta) {
                case 100:
                    grp = MyConstantes.IMPORTADOS;
                    break;
                case 101:
                    grp = MyConstantes.COM_ENVIO_AUTORIZADO;
                    break;
                case 102:
                    grp = MyConstantes.AGUARDANDO_PROCESSAMENTO_ESOCIAL;
                    break;
                case 201:
                case 202:
                    grp = MyConstantes.RECEBIDOS_COM_SUCESSO;
                    break;
                default:
                    grp = MyConstantes.COM_ERRO;
                    break;
            }


            this.totaisSituacao[grp] += element.quantidade;
        });
    }

    navegarImportados() {
        this._myScopeService.filtrarImportados = true;
        this.navegar();
    }

    navegarAutorizados() {
        this._myScopeService.filtrarAutorizados = true;
        this.navegar();
    }

    navegarEnviados() {
        this._myScopeService.filtrarEnviados = true;
        this.navegar();
    }

    navegarErros() {
        this._myScopeService.filtrarErros = true;
        this.navegar();
    }

    navegarRelatorioErros() {
        this._router.navigate(['relatorio-erros']);
    }

    navegar() {
        this._router.navigate(['pendentes']);
    }

}
