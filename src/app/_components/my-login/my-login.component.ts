import {
    Component,
    OnInit,
    OnDestroy,
    Inject,
    Injectable
} from '@angular/core';

import {
    Router,
    ActivatedRoute
} from '@angular/router';

import {
    MyAlertService,
    MyAuthService,
    MyMenuService,
    MyOverlayService,
    MyTokenService,
    MyLocalStorageService,
    MyScopeService
} from './../../_services/index';

import './../../_types/my_types';

import {APP_CONFIG, AppConfig} from './../../app.config';
import {MyJwtHelper} from "../../_helpers/my-jwtHelper";


@Component({
    moduleId: module.id,
    templateUrl: './my-login.component.html',
    styleUrls: ['./my-login.component.css']
})
export class MyLoginComponent implements OnInit {

    usuario: Usuario = this.emptyUser();
    orgaoEmpresa: string;

    loading = false;
    returnUrl: string;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private _myAuthService: MyAuthService,
        private _myMenuService: MyMenuService,
        private _myAlertService: MyAlertService,
        private _myOverlayService: MyOverlayService,
        private _myTokenService: MyTokenService,
        private _myScopeService: MyScopeService,
        private _myLocalStorageService: MyLocalStorageService,
        private _myJwtHelper: MyJwtHelper
    ) {
    }

    ngOnInit() {
        // reset login status
        this.logout();

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    login() {
        this._myOverlayService.mostrar();
        this._myAuthService.login(this.usuario.username, this.usuario.password)
            .subscribe(
            data => {
                if (data.status === 200) {
                    const authTokenLogin = data.body.token;
                    this._myTokenService.saveToken(authTokenLogin);
                    this._myOverlayService.ocultar();
                    this._myAlertService.clear();
                    const dadosToken = this._myJwtHelper.decodeToken(authTokenLogin);
                    this._myScopeService.setUsuarioCorrente(this.usuario.username, dadosToken.roles[0], this.usuario.username, '');

                    this.orgaoEmpresa = this._myLocalStorageService.primeiraMaiuscula(this._myScopeService.usuarioCorrente.orgao);
                    this._myLocalStorageService.save('epgd', btoa(this.orgaoEmpresa));
                    this._myLocalStorageService.save('usrnm', btoa(this.usuario.username));
                    this._myLocalStorageService.save('usrcnum', btoa(dadosToken.nrInsc));

                    this.router.navigate([this.returnUrl]);
                } else {
                    this._myOverlayService.ocultar();
                    this._myAlertService.error('Usuário e/ou senha não conferem');
                }
            },
            error => {
                this._myOverlayService.ocultar();
                this._myAlertService.error('Erro ao autenticar acesso: Verifique suas credenciais.');
                this.router.navigate(['/login']);
            });
    }

    logout() {
        this._myMenuService.ocultar();
        this._myAuthService.logout();
    }

    emptyUser() {
        return {
            username: null,
            password: null,
            firstName: null,
            lastName: null,
            orgao: null
        };
    }
}
