import {Component, OnInit, Inject} from '@angular/core';
import {
    DOCUMENT
} from '@angular/platform-browser';

import {
    MyScopeService,
    MyHttpService,
    MyLocalStorageService
} from '../../_services/index';

import { MyBackendRoutes } from './../../_config/index';


import '../../_types/my_types';

@Component({
    templateUrl: './my-envio-eventos.component.html',
    styleUrls: ['./my-envio-eventos.component.css'],
})
export class MyEnvioEventosComponent implements OnInit {

    private file = Array();

    usuarioCorrente: UsuarioCorrente;
    language = 'markup';
    mostrarArquivo = false;
    hideAlertMessage = true;
    desabilitarBotao = false;
    hideFeedBack = false;
    contentFiles = '';


    constructor(
        private _myScopeService: MyScopeService,
        private _myLocalStorageService: MyLocalStorageService,
        private _myHttpService: MyHttpService) {
        this.usuarioCorrente = this._myScopeService.usuarioCorrente;
        this.hideAlertMessage = false;
        this.desabilitarBotao = true;
        this.hideFeedBack = false;
    }

    ngOnInit() {
    }

    fileUpload(event) {
        var me = this;
        this.file = new Array();
        this.contentFiles = '';
        this.hideFeedBack = true;
        for (var i = 0; i < event.srcElement.files.length; i++) { //for multiple files          
            (function(file) {
                if(me.contentFiles =='')
                    me.contentFiles += file.name;
                else
                    me.contentFiles += '<br>' + file.name;
                var reader = new FileReader();  
                reader.onload = function(e) {  
                    // get file content  
                    me.file.push(reader.result);
                }
                reader.readAsText(file);
            })(event.srcElement.files[i]);
        }
        this.desabilitarBotao = false;
    }

    botaoHabilitar(){
        return this.desabilitarBotao;
    }

    enviarEvento() {
        const path = this._myLocalStorageService.getBaseUrl() + MyBackendRoutes.ARQUIVO_EVENTOS;
        const payload = [];
        //xml
        if (!this.desabilitarBotao){
            this.desabilitarBotao = true;

            this.file.forEach((file) => payload.push({xml: file}));

            this._myHttpService.post(path, payload)
                .subscribe((response: any) => {
                    if (response.status === 200) {
                        //envio realizado com sucesso
                        this.hideAlertMessage = true;
                        this.mostrarArquivo = false;
                        setTimeout( () => {
                            this.hideAlertMessage = false;
                            this.contentFiles = '';
                            this.hideFeedBack = false;
                        }, 2000);
                    } else {
                        console.log('Erro ao enviar evento');
                    }
                });
        }
    }
}
