import {Component, OnInit, Inject} from '@angular/core';
import {
    DOCUMENT
} from '@angular/platform-browser';

import {
    MyScopeService,
    MyHttpService,
    MyLocalStorageService
} from '../../_services/index';

import { MyBackendRoutes } from '../../_config/index';


import '../../_types/my_types';


@Component({
    templateUrl: './my-relatorios.component.html',
    styleUrls: ['./my-relatorios.component.css'],
})
export class MyRelatoriosComponent implements OnInit {

    usuarioCorrente: UsuarioCorrente;

    constructor(
        private _myScopeService: MyScopeService,
        private _myLocalStorageService: MyLocalStorageService,
        private _myHttpService: MyHttpService) {
        this.usuarioCorrente = this._myScopeService.usuarioCorrente;
    }

    ngOnInit() {
    }
}
