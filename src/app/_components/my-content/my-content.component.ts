import { MyDashboardComponent } from './../my-dashboard/my-dashboard.component';
import { Component, OnInit } from '@angular/core';
import { MyVersion } from './../../_config/my-constantes.config';

@Component({
  selector: 'app-my-content',
  templateUrl: './my-content.component.html',
  styleUrls: [ './my-content.component.css' ],
})
export class MyContentComponent implements OnInit {

  public autenticado = false;
  public versao: string;
  public dataVersao: string;

  constructor() {
  }

  ngOnInit() {
    this.versao = MyVersion.MAJOR + '.' + MyVersion.MINOR + '.' + MyVersion.PATCH;
    this.dataVersao = MyVersion.DATE;
  }
}
