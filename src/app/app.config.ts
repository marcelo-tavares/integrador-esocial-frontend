import { InjectionToken } from '@angular/core';

export class AppConfig {
  BACKEND_URL: string;
  MANTER_ALERT_APOS_NAVEGAR = true;
}

export let APP_CONFIG = new InjectionToken<AppConfig>('app.config');

//teste de push
