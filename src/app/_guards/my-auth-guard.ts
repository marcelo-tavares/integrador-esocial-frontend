import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { MyTokenService } from './../_services/index';

@Injectable()
export class MyAuthGuard implements CanActivate {

    constructor(private router: Router,
                private _myTokenService: MyTokenService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const token = this._myTokenService.getToken();
        if (token) {
            return true; // autenticado então retorna true
        }

        // não autenticado, então redireciona para a página de login com a url para retornar
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
        return false;
    }
}
