import { Component, OnInit } from '@angular/core';
import { MyLocalStorageService } from './../../_services/my-localstorage.service';
import { DoCheck } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'my-logo',
  templateUrl: './my-logo.component.html',
  styleUrls: ['./my-logo.component.css']
})
export class MyLogoComponent implements OnInit, DoCheck {

  private empresa: string;
  public arquivoLogo: string;

  constructor(
    private _myLocalStorageService: MyLocalStorageService) {
  }

  ngOnInit() {
  }

  ngDoCheck() {
    this.empresa = this._myLocalStorageService.get('epgd');
    if (this.empresa === null) {
      this.empresa = '';
    } else {
      this.empresa = atob(this.empresa);
    }
    this.arquivoLogo = 'logo' + this.empresa + 'Branco.png';
  }
}
