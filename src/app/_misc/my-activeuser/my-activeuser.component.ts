import { Component, OnInit, DoCheck } from '@angular/core';

import {
    MyMenuService,
    MyScopeService,
    MyAuthService,
    MyLocalStorageService
} from './../../_services/index';

@Component({
    selector: 'my-activeuser',
    templateUrl: './my-activeuser.component.html',
    styleUrls: ['./my-activeuser.component.css']
})
export class MyActiveuserComponent implements OnInit, DoCheck {

    autenticado: boolean;
    usuarioCorrente: UsuarioCorrente;

    constructor(private _myAuthService: MyAuthService,
                private _myMenuService: MyMenuService,
                private _myLocalStorageService: MyLocalStorageService,
                private _myScopeService: MyScopeService) {

        this.usuarioCorrente = {username: '', orgao: '', nomecompleto: ''};
    }

    ngOnInit() {
        this._myMenuService.getMessage()
            .subscribe(resultado => {
                this.autenticado = resultado;
                if (this.autenticado) {
                    this.usuarioCorrente.orgao = atob(this._myLocalStorageService.get('epgd')).toUpperCase();
                }
            });
    }

    ngDoCheck() {
        this.usuarioCorrente.nomecompleto = atob(this._myLocalStorageService.get('nmcplt'));
    }
}
