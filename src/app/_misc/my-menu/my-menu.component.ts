import { Component, OnInit } from '@angular/core';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import {
    Router,
    ActivatedRoute,
    NavigationStart
} from '@angular/router';


import { MyMenuService } from './../../_services/index';
import { MyTokenService } from './../../_services/my-token.service';

@Component({
    selector: 'my-menu',
    templateUrl: './my-menu.component.html',
    styleUrls: ['./my-menu.component.css'],
    providers: [NgbDropdownConfig]
})
export class MyMenuComponent implements OnInit {

    autenticado: boolean;
    show: boolean;

    constructor(
        private _NgbDropDownConfig: NgbDropdownConfig,
        private _myMenuService: MyMenuService,
        private _myTokenService: MyTokenService,
        private _router: Router) {

        _NgbDropDownConfig.placement = 'bottom-right';
    }

    ngOnInit() {
        this._myMenuService.getMessage()
            .subscribe(resultado => {
                this.autenticado = resultado;
            });

        this.show = false;

        this._router.events
            .subscribe(event => {
                if (event instanceof NavigationStart) {
                    if (this._myTokenService.getToken())
                        this._myMenuService.mostrar()
                }
            });
    }

    navegar(rota) {
        this._router.navigate([rota]);
    }

}
