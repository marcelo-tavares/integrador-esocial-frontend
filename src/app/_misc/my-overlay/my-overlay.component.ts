import { Component, OnInit } from '@angular/core';

import { MyOverlayService } from './../../_services';

@Component({
    moduleId: module.id,
    selector: 'my-overlay',
    templateUrl: './my-overlay.component.html',
    styleUrls: ['./my-overlay.component.css']
})

export class MyOverlayComponent {
    isLoading: boolean;

    constructor(private _myOverlayService: MyOverlayService) { }

    ngOnInit() {
        this._myOverlayService.getMessage()
            .subscribe(isLoading => { 
                this.isLoading = isLoading; 
            });
    }

    close() {
        this.isLoading = false;
    }
}