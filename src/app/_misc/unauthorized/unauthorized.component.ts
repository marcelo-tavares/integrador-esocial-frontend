import { Component, OnInit } from '@angular/core';

import { MyOverlayService } from './../../_services/index';

@Component({
  templateUrl: './unauthorized.component.html',
  styleUrls: ['./unauthorized.component.css']
})
export class UnauthorizedComponent implements OnInit {

  constructor(private _myOverlayService: MyOverlayService) { }

  ngOnInit() {
    this._myOverlayService.ocultar();
  }

}
