import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';

import { MyAlertService } from '../../_services/index';

@Component({
    moduleId: module.id,
    selector: 'my-alert',
    templateUrl: 'my-alert.component.html'
})

export class MyAlertComponent implements OnInit {
    message: any;

    constructor(
        private _myAlertService: MyAlertService,
        private _router: Router ) { }

    ngOnInit() {
        this._myAlertService.getMessage()
            .subscribe(message => {
                this.message = message;
            });

        // Limpar mensagem de alerta ao mudar rota
        this._router.events.subscribe(event => {
            if (event instanceof NavigationStart) {
                this._myAlertService.clearAfterNavigation();
            }
        });

    }

    closeAlert() {
        this.message = null;
    }
}



