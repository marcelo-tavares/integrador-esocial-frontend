import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'myIndicativoContribuicaoPrevidenciariaPatronal' })
export class MyIndicativoContribuicaoPrevidenciariaPatronalPipe implements PipeTransform {
    transform(value: any) {
        switch (value) {
            case 1: {
                return '1 - Integralmente substituída';
            }
            case 2: {
                return '2 - Parcialmente substituída';
            }
            default: {
                return 'Indicativo Inválido';
            }
        }
    }
}
