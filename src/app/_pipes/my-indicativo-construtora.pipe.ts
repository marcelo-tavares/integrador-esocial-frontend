import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'myIndicativoConstrutora' })
export class MyIndicativoConstrutoraPipe implements PipeTransform {
    transform(value: any) {
        switch (value) {
            case 0: {
                return '0 - Não é Construtora';
            }
            case 1: {
                return '1 - Empresa Construtora';
            }
            default: {
                return 'Indicativo Inválido';
            }
        }
    }
}
