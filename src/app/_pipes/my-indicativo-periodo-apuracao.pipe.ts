import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'myIndicativoPeriodoApuracao' })
export class MyIndicativoPeriodoApuracaoPipe implements PipeTransform {
    transform(value: any) {
        switch (value) {
            case 1: {
                return '1 - Mensal';
            }
            case 2: {
                return '2 - Anual (13° salário)';
            }
            default: {
                return 'Indicativo Inválido';
            }
        }
    }
}
