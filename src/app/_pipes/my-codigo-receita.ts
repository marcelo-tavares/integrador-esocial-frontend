import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'myCodigoReceita' })
export class MyCodigoReceitaPipe implements PipeTransform {
    transform(value: any) {
        switch (value) {
            case '047301': {
                return '0473-01 - IRRF - Residentes Fiscais no Exterior';
            }
            case '056107': {
                return '0561-07 - IRRF Mensal, 13° salário e Férias sobre Trabalho Assalariado no país ou ausente no exterior a serviço do país, exceto se contratado por Empregador Doméstico ou por Segurado Especial sujeito a recolhimento unificado';
            }
            case '056108': {
                return '0561-08 - IRRF Mensal, 13° salário e Férias sobre Trabalho Assalariado no país ou ausente no exterior a serviço do país, Empregado Doméstico ou Trabalhador contratado por Segurado Especial sujeito a recolhimento unificado';
            }
            case '056109': {
                return '0561-09 - IRRF 13° salário na rescisão de contrato de trabalho relativo a empregador sujeito a recolhimento unificado';
            }
            case '056110': {
                return '0561-10 - IRRF - Empregado doméstico - 13º salário';
            }
            case '056111': {
                return '0561-11 - IRRF - Empregado/Trabalhador Rural - Segurado Especial';
            }
            case '056112': {
                return '0561-12 - IRRF - Empregado/Trabalhador Rural - Segurado Especial 13° salário';
            }
            case '056113': {
                return '0561-13 - IRRF - Empregado/Trabalhador Rural - Segurado Especial 13° salário rescisório';
            }
            case '058806': {
                return '0588-06 - IRRF sobre Rendimento do trabalho sem vínculo empregatício';
            }
            case '061001': {
                return '0610-01 - IRRF sobre Rendimentos relativos a prestação de serviços de transporte rodoviário internacional de carga, pagos a transportador autônomo PF residente no Paraguai';
            }
            case '328006': {
                return '3280-06 - IRRF sobre Serviços Prestados por associados de cooperativas de trabalho';
            }
            case '3533': {
                return '3533 - Proventos de Aposentadoria, Reserva, Reforma ou Pensão Pagos por Previdência Pública';
            }
            case '356201': {
                return '3562-01 - IRRF sobre Participação dos trabalhadores em Lucros ou Resultados (PLR)';
            }
            default: {
                return 'Código de Receita Inválido';
            }
        }
    }
}
