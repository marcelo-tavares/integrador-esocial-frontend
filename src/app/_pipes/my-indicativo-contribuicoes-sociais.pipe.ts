import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'myIndicativoContribuicoesSociais' })
export class MyIndicativoContribuicoesSociaisPipe implements PipeTransform {
    transform(value: any) {
        switch (value) {
            case 1: {
                return '1 - Há informações com apuração de contribuições sociais';
            }
            case 2: {
                return '2 - Há movimento porém sem apuração de contribuições sociais';
            }
            case 3: {
                return '3 - Não há movimento no período informado';
            }
            default: {
                return 'Indicativo Inválido';
            }
        }
    }
}
