import { Pipe, PipeTransform } from '@angular/core';

import * as moment from 'moment';
import 'moment/locale/pt-br';

/*
    Transforma a competencia do formato YYYY-MM
    para MMM/YYYY
*/
@Pipe({ name: 'myCompetencia' })
export class MyCompetenciaPipe implements PipeTransform {
    transform(value: any) {
        if (value == null) {
            return 'Competencia inválida';
        } else {
            const ano = value.substring(0, 4);
            const mes = value.substring(5);
            switch (mes) {
                case '01': return 'Jan/' + ano;
                case '02': return 'Fev/' + ano;
                case '03': return 'Mar/' + ano;
                case '04': return 'Abr/' + ano;
                case '05': return 'Mai/' + ano;
                case '06': return 'Jun/' + ano;
                case '07': return 'Jul/' + ano;
                case '08': return 'Ago/' + ano;
                case '09': return 'Set/' + ano;
                case '10': return 'Out/' + ano;
                case '11': return 'Nov/' + ano;
                case '12': return 'Dez/' + ano;
                case '13': return '13º/' + ano;
            }
        }
    }
}
