import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'myIndicativoImpostoRenda' })
export class MyIndicativoImpostoRendaPipe implements PipeTransform {
    transform(value: any) {
        switch (value) {
            case 1: {
                return '1 - Há informações de Imposto de Renda Retido na Fonte';
            }
            case 2: {
                return '2 - Há movimento, porém não há informações de Imposto de Renda Retido na Fonte';
            }
            case 3: {
                return '3 - Não há movimento no período informado';
            }
            default: {
                return 'Indicativo Inválido';
            }
        }
    }
}
