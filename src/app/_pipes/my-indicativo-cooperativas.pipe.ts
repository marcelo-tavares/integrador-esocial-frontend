import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'myIndicativoCooperativas' })
export class MyIndicativoCooperativasPipe implements PipeTransform {
    transform(value: any) {
        switch (value) {
            case 0: {
                return '0 - Não é cooperativa';
            }
            case 1: {
                return '1 - Cooperativa de Trabalho';
            }
            case 2: {
                return '2 - Cooperativa de Produção';
            }
            case 3: {
                return '3 - Outras Cooperativas';
            }
            default: {
                return 'Indicativo Inválido';
            }
        }
    }
}
