import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'myIndicativoAquisicao' })
export class MyIndicativoAquisicaoPipe implements PipeTransform {
    transform(value: any) {
        switch (value) {
            case 1: {
                return '1 - Aquisição da produção de produtor rural pessoa física ou segurado especial em geral';
            }
            case 2: {
                return '2 - Aquisição da produção de produtor rural pessoa física ou segurado especial em geral por Entidade do PAA';
            }
            case 3: {
                return '3 - Aquisição da produção de produtor rural pessoa jurídica por Entidade do PAA';
            }
            default: {
                return 'Indicativo Inválido';
            }
        }
    }
}
