import { Pipe, PipeTransform } from '@angular/core';

import * as vkbeautify from 'vkbeautify';

/*
 * Formata XML
 * Usage:
 *   value
 * Example:
 *   {{ evento.xml | myXml }}
*/
@Pipe({name: 'myXml'})
export class MyXmlPipe implements PipeTransform {
    transform(value: string): string {
        return vkbeautify.xml(value);
    }
}
