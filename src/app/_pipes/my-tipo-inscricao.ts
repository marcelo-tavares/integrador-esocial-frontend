import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'myTipoInscricao' })
export class MyTipoInscricaoPipe implements PipeTransform {
    transform(value: any) {
        switch (value) {
            case 1: {
                return '1 - CNPJ';
            }
            case 2: {
                return '2 - CPF';
            }
            default: {
                return 'Tipo de Inscrição Inválido';
            }
        }
    }
}
