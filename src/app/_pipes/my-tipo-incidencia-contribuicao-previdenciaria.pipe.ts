import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'myTipoIncidenciaContribuicaoPrevidenciaria' })
export class MyTipoIncidenciaContribuicaoPrevidenciariaPipe implements PipeTransform {
    transform(value: any) {
        switch (value) {
            case 1: {
                return '1 - Normal';
            }
            case 2: {
                return '2 - Ativ. Concomitante';
            }
            case 9: {
                return '9 - Substituída ou Isenta';
            }
            default: {
                return 'Indicativo Inválido';
            }
        }
    }
}
