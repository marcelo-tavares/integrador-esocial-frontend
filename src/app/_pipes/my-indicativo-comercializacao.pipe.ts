import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'myIndicativoComercializacao' })
export class MyIndicativoComercializacaoPipe implements PipeTransform {
    transform(value: any) {
        switch (value) {
            case 2: {
                return '2 - Comercialização da Produção efetuada diretamente no varejo a consumidor final ou a outro produtor rural pessoa física por Produtor Rural Pessoa Física, inclusive por Segurado Especial ou por Pessoa Física não produtor rural';
            }
            case 3: {
                return '3 - Comercialização da Produção por Prod. Rural PF/Seg. Especial - Vendas a PJ (exceto Entidade inscrita no Programa de Aquisição de Alimentos - PAA) ou a Intermediário PF';
            }
            case 8: {
                return '8 - Comercialização da Produção da Pessoa Física/Segurado Especial para Entidade inscrita no Programa de Aquisição de Alimentos - PAA';
            }
            case 9: {
                return '9 - Comercialização da Produção no Mercado Externo';
            }
            default: {
                return 'Indicativo Inválido';
            }
        }
    }
}
