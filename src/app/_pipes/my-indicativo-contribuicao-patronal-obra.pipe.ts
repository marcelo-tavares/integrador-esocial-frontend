import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'myIndicativoContribuicaoPatronalObra' })
export class MyIndicativoContribuicaoPatronalObraPipe implements PipeTransform {
    transform(value: any) {
        switch (value) {
            case 1: {
                return '1 - Contribuição Patronal Substituída';
            }
            case 2: {
                return '2 - Contribuição Patronal Não Substituída';
            }
            default: {
                return 'Indicativo Inválido';
            }
        }
    }
}
