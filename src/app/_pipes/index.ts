// start:ng42.barrel
export * from './my-date.pipe';
export * from './my-xml.pipe';
export * from './my-search-filter.pipe';
export * from './my-competencia.pipe';
export * from './my-tipo-inscricao';
export * from './my-codigo-receita';
export * from './my-indicativo-imposto-renda';
export * from './my-indicativo-periodo-apuracao.pipe';
export * from './my-indicativo-contribuicoes-sociais.pipe';
export * from './my-indicativo-cooperativas.pipe';
export * from './my-indicativo-construtora.pipe';
export * from './my-indicativo-contribuicao-previdenciaria-patronal.pipe';
export * from './my-indicativo-contribuicao-patronal-obra.pipe';
export * from './my-tipo-incidencia-contribuicao-previdenciaria.pipe';
export * from './my-indicativo-aquisicao.pipe';
export * from './my-indicativo-comercializacao.pipe';
export * from './my-cpf.pipe';
export * from './my-cnpj.pipe';
// end:ng42.barrel

