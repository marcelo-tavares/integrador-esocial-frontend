import { Pipe, PipeTransform } from '@angular/core';

import * as moment from 'moment';
import 'moment/locale/pt-br';

/*
 * Formata datas no formato pt-br
 * Usage:
 *   value
 * Example:
 *   {{ evento.dataGeracao | myDate }}
 *   Se value = '2017-09-28 01:06:00'
 *   retorna '28/09/2017 01:06:00'
*/
@Pipe({ name: 'myDate' })
export class MyDatePipe implements PipeTransform {
    transform(value: any) {
        let myDate = '';
        if (value == null) {
            myDate = 'Data inválida';
        } else {
            const data = new Date(value.$date);
            myDate = moment(data).format('L') + ' ' + moment(data).format('LTS');
        }
        return myDate;
    }
}
