import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'MySearchfilter'
})

@Injectable()
export class MySearchFilterPipe implements PipeTransform {
    transform(items: any[], field: string, value: string): any[] {
       if (!value) {
            return items;
        }
        return items.filter(it => it[field].indexOf(value) > -1);
    }
}
