import { Directive, ElementRef, OnInit } from '@angular/core';

import { MyAuthService } from './../../_services/index';

@Directive({
    selector: '[visibleIfAuth]'
})
export class IfAuthDirective {

    constructor(private _elemento: ElementRef,
        private _myAuthService: MyAuthService) { }

    OnInit() {
        /*
        if (this._myAuthService.loggedIn()) {
            this._elemento.nativeElement.style.display = 'inline-block!important';
        } else {
            this._elemento.nativeElement.style.display = 'none!important';
        }
        */
    }
}
