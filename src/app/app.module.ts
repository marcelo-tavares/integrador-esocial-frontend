import { BrowserModule } from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';
import { TextMaskModule } from 'angular2-text-mask';

import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Ng2Webstorage } from 'ngx-webstorage';
import { SelectModule } from 'ng2-select';

// My Modules
import { MyRoutesModule,
         MyNgBsModule } from './_modules/index';

// My Components
import { AppComponent } from './app.component';
import { MyContentComponent,
         MyDashboardComponent,
         MyLoginComponent,
         MyQualificacaoComponent,
         MyPendentesComponent,
         MyEventosTrabalhadorComponent,
         MyEnvioEventosComponent,
         MyPainelComponent,
         MyConfigComponent,
         MyPainelDetalheComponent,
         MyPainelListComponent,
         MyFolhaPagamentoComponent,
         MyRelatoriosComponent,
         MyAgrupamentoFolhaComponent,
         MyDetalhePagamentoComponent,
         MyApuracaoConsolidadaImpostoRendaComponent,
         MyApuracaoConsolidadaFundoDeGarantiaComponent,
         MyApuracaoConsolidadaContribuicoesSociaisComponent,
         MyRelatorioErrosComponent} from './_components/index';


// Miscelâneas
import { MyAlertComponent,
         MyOverlayComponent,
         MyMenuComponent,
         MyActiveuserComponent,
         UnauthorizedComponent } from './_misc/index';

// My Guards
import { MyAuthGuard } from './_guards/index';

// My Services
import { MyValidateService,
    MyHttpService,
    MyAlertService,
    MyOverlayService,
    MyMenuService,
    MyAuthService,
    MyAuthInterceptor,
    MyTokenService,
    MyScopeService,
    MyLocalStorageService,
    MyArrayService,
    MyActiveUserService,
    MyEventsService,
    MyFolhasService} from './_services/index';

// My Pipes
import {
    MyDatePipe,
    MyXmlPipe,
    MySearchFilterPipe,
    MyCompetenciaPipe,
    MyTipoInscricaoPipe,
    MyCodigoReceitaPipe,
    MyIndicativoImpostoRendaPipe,
    MyIndicativoPeriodoApuracaoPipe,
    MyIndicativoContribuicoesSociaisPipe,
    MyIndicativoCooperativasPipe,
    MyIndicativoConstrutoraPipe,
    MyIndicativoContribuicaoPrevidenciariaPatronalPipe,
    MyIndicativoContribuicaoPatronalObraPipe,
    MyTipoIncidenciaContribuicaoPrevidenciariaPipe,
    MyIndicativoAquisicaoPipe,
    MyIndicativoComercializacaoPipe,
    MyCpfPipe,
    MyCnpjPipe} from './_pipes/index';


// Config
import { MyBackendRoutes } from './_config/index';
import { MyESocialReturnCodes } from './_config/index';

// Helpers
import { MyJwtHelper, MyFormatErrorMensageHelper } from './_helpers/index';

// Diretivas
import { IfAuthDirective } from './_directives/index';

// Outros
import { APP_CONFIG } from './app.config';
import { environment } from '../environments/environment';
import { MyLogoComponent } from './_misc/my-logo/my-logo.component';
import {Ng2BRPipesModule} from "ng2-brpipes";
import { PrismModule } from '@ngx-prism/core';

import localeBR from '@angular/common/locales/pt';
import {registerLocaleData} from '@angular/common';

registerLocaleData(localeBR);

@NgModule({
    declarations: [
        AppComponent,
        MyContentComponent,
        MyDashboardComponent,
        MyLoginComponent,
        MyQualificacaoComponent,
        MyPendentesComponent,
        MyEventosTrabalhadorComponent,
        MyEnvioEventosComponent,
        MyPainelComponent,
        MyPainelListComponent,
        MyPainelDetalheComponent,
        MyConfigComponent,
        MyFolhaPagamentoComponent,
        MyRelatoriosComponent,
        MyAgrupamentoFolhaComponent,
        MyDetalhePagamentoComponent,
        MyRelatorioErrosComponent,
        MyApuracaoConsolidadaImpostoRendaComponent,
        MyApuracaoConsolidadaFundoDeGarantiaComponent,
        MyApuracaoConsolidadaContribuicoesSociaisComponent,

        MyAlertComponent,
        MyMenuComponent,
        MyOverlayComponent,
        MyActiveuserComponent,
        UnauthorizedComponent,

        MyDatePipe,
        MyCompetenciaPipe,
        MyXmlPipe,
        MySearchFilterPipe,
        MyTipoInscricaoPipe,
        MyCodigoReceitaPipe,
        MyIndicativoImpostoRendaPipe,
        MyIndicativoPeriodoApuracaoPipe,
        MyIndicativoContribuicoesSociaisPipe,
        MyIndicativoCooperativasPipe,
        MyIndicativoConstrutoraPipe,
        MyIndicativoContribuicaoPrevidenciariaPatronalPipe,
        MyIndicativoContribuicaoPatronalObraPipe,
        MyTipoIncidenciaContribuicaoPrevidenciariaPipe,
        MyIndicativoAquisicaoPipe,
        MyIndicativoComercializacaoPipe,
        MyCpfPipe,
        MyCnpjPipe,

        IfAuthDirective,

        MyLogoComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        TextMaskModule,
        FormsModule,
        RouterModule.forRoot([
            {
                path: 'dashboard',
                component: MyDashboardComponent
            }
        ]),
        Ng2Webstorage.forRoot(
            {
                prefix: 'es',
                separator: '.'
            }
        ),
        MyRoutesModule,
        MyNgBsModule,
        SelectModule,
        Ng2BRPipesModule,
        PrismModule
    ],
    providers: [
        MyAlertService,
        MyOverlayService,
        MyAuthGuard,
        MyAuthService,
        MyHttpService,
        MyEventsService,
        MyFolhasService,
        MyValidateService,
        MyMenuService,
        MyTokenService,
        MyLocalStorageService,
        MyActiveUserService,
        MyArrayService,
        MyScopeService,
        MyDatePipe,
        MyJwtHelper,
        MyFormatErrorMensageHelper,

        { provide: LOCALE_ID, useValue: 'pt-BR' },

        // Interceptor Http
        {
            provide: HTTP_INTERCEPTORS,
            useClass: MyAuthInterceptor,
            multi: true,
        },

        // App Config
        {
            provide: APP_CONFIG,
            useValue: environment.APP_CONFIG_IMPL
          }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
