import { MyOverlayService } from './../_services/index';

describe('MyOverlayService: ', () => {

    let myOverlayService: MyOverlayService;
    let isLoading: boolean;

    beforeEach(() => {
        myOverlayService = new MyOverlayService();
    });

    it('Método "mostrar" deve provocar a exibição do overlay', () => {
        isLoading = true; 
        expect(myOverlayService).toBeDefined();
        myOverlayService.getMessage()
            .subscribe(resultado => {
                isLoading = resultado;
                expect(isLoading).toBe(true);
            });

            myOverlayService.mostrar();
    });

    it('Método "ocultar" deve provocar a ocultação do overlay', () => {
        isLoading = false;
        expect(myOverlayService).toBeDefined();
        myOverlayService.getMessage()
            .subscribe(resultado => {
                isLoading = resultado;
                expect(isLoading).toBe(false);
            });

            myOverlayService.ocultar();
    });

});
