import { MyAlertService } from './../_services/index';

describe('MyAlertService: ', () => {
    
        let myAlertService: MyAlertService;
        let myAlertComponentStub: MyAlertComponentStub;
        let autenticado: boolean;
    
        beforeEach(() => {
            myAlertService = new MyAlertService();
            myAlertComponentStub = new MyAlertComponentStub(myAlertService);
        });
    
        it('Método "success" com manterAposMudarRota=false (default) deve exibir uma mensagem de sucesso e sumir depois de navegar', () => {
            expect(myAlertComponentStub).toBeDefined();
            expect(myAlertComponentStub.message).toBeNull();
            myAlertService.success('mensagem de sucesso');
            expect(myAlertComponentStub.message.text).toEqual('mensagem de sucesso');
            expect(myAlertComponentStub.message.type).toEqual('success');
            myAlertService.clearAfterNavigation();
            expect(myAlertComponentStub.message).toBeUndefined();            
        });
    
        it('Método "success" com manterAposMudarRota=true deve exibir uma mensagem de sucesso e permanecer depois de navegar', () => {
            expect(myAlertComponentStub).toBeDefined();
            expect(myAlertComponentStub.message).toBeNull();
            myAlertService.success('mensagem de sucesso', true);
            expect(myAlertComponentStub.message.text).toEqual('mensagem de sucesso');
            expect(myAlertComponentStub.message.type).toEqual('success');
            myAlertService.clearAfterNavigation();
            expect(myAlertComponentStub.message.text).toEqual('mensagem de sucesso');
            expect(myAlertComponentStub.message.type).toEqual('success');
        });
    
        it('Método "warning" com manterAposMudarRota=false (default) deve exibir uma mensagem de aviso e sumir depois de navegar', () => {
            expect(myAlertComponentStub).toBeDefined();
            expect(myAlertComponentStub.message).toBeNull();
            myAlertService.warning('mensagem de aviso');
            expect(myAlertComponentStub.message.text).toEqual('mensagem de aviso');
            expect(myAlertComponentStub.message.type).toEqual('warning');
            myAlertService.clearAfterNavigation();
            expect(myAlertComponentStub.message).toBeUndefined();            
        });
    
        it('Método "warning" com manterAposMudarRota=true deve exibir uma mensagem de aviso e permenecer depois de navegar', () => {
            expect(myAlertComponentStub).toBeDefined();
            expect(myAlertComponentStub.message).toBeNull();
            myAlertService.warning('mensagem de aviso', true);
            expect(myAlertComponentStub.message.text).toEqual('mensagem de aviso');
            expect(myAlertComponentStub.message.type).toEqual('warning');
            myAlertService.clearAfterNavigation();
            expect(myAlertComponentStub.message.text).toEqual('mensagem de aviso');
            expect(myAlertComponentStub.message.type).toEqual('warning');
        });
    
        it('Método "error" com manterAposMudarRota=false (default) deve exibir uma mensagem de erro e sumir depois de navegar', () => {
            expect(myAlertComponentStub).toBeDefined();
            expect(myAlertComponentStub.message).toBeNull();
            myAlertService.error('mensagem de erro');
            expect(myAlertComponentStub.message.text).toEqual('mensagem de erro');
            expect(myAlertComponentStub.message.type).toEqual('danger');
            myAlertService.clearAfterNavigation();
            expect(myAlertComponentStub.message).toBeUndefined();            
        });
    
        it('Método "error" com manterAposMudarRota=true deve exibir uma mensagem de erro e permanecer depois de navegar', () => {
            expect(myAlertComponentStub).toBeDefined();
            expect(myAlertComponentStub.message).toBeNull();
            myAlertService.error('mensagem de erro', true);
            expect(myAlertComponentStub.message.text).toEqual('mensagem de erro');
            expect(myAlertComponentStub.message.type).toEqual('danger');
            myAlertService.clearAfterNavigation();
            expect(myAlertComponentStub.message.text).toEqual('mensagem de erro');
            expect(myAlertComponentStub.message.type).toEqual('danger');
        });
    
        it('Método "clear" deve exibir ocultar as mensagens', () => {
            expect(myAlertComponentStub).toBeDefined();
            expect(myAlertComponentStub.message).toBeNull();
            myAlertService.error('mensagem a ser ocultada', true);
            expect(myAlertComponentStub.message.text).toEqual('mensagem a ser ocultada');
            expect(myAlertComponentStub.message.type).toEqual('danger');
            myAlertService.clear();
            expect(myAlertComponentStub.message).toBeUndefined();
            expect(myAlertService.manterAposMudarRota).toBeFalsy();
        });
    
    });

class MyAlertComponentStub {
    public message: any;

    constructor(private _myAlertService: MyAlertService) {
        this._myAlertService.getMessage()
            .subscribe(message => {
                this.message = message;
            });

        this.closeAlert();
    }

    closeAlert() {
        this.message = null;
    }
}

