import { FormsModule } from '@angular/forms';

import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import {
    TestBed,
    ComponentFixture,
    fakeAsync,
    tick,
    async
} from '@angular/core/testing';

import {
    Router,
    ActivatedRoute
} from '@angular/router';

import {
    MyLoginComponent
} from './../_components/index';

import {
    MyAlertService,
    MyMenuService,
    MyAuthService,
    MyHttpService,
    MyTokenService,
    MyOverlayService,
} from './../_services/index';

import {
    MyJwtHelper
} from './../_helpers/index';

import {
    RouterStub,
    ActivatedRouteStub,
    MyAuthServiceStub,
    MyMenuServiceStub,
    MyAlertServiceStub,
    MyHttpServiceStub,
    MyOverlayServiceStub
} from './stubs/index';

describe('MyLoginComponent: ', () => {

    let _rota: ActivatedRoute;
    let router: Router;
    let _myAuthService: MyAuthService;
    let _myMenuService: MyMenuService;
    let _myAlertService: MyAlertService;
    let _myOverlayService: MyOverlayService;
    let component: MyLoginComponent;
    let fixture: ComponentFixture<MyLoginComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                FormsModule
            ],
            declarations: [
                MyLoginComponent
            ],
            providers: [
                MyTokenService,
                MyJwtHelper,
                { provide: MyAlertService, useValue: new MyAlertServiceStub() },
                { provide: MyAuthService, useValue: new MyAuthServiceStub() },
                { provide: MyMenuService, useValue: new MyMenuServiceStub() },
                { provide: MyOverlayService, useValue: new MyOverlayServiceStub() },
                { provide: MyHttpService, useValue: new MyHttpServiceStub() },
                { provide: Router, useValue: new RouterStub() },
                { provide: ActivatedRoute, useValue: new ActivatedRouteStub() }
            ]
        })
            .compileComponents()
            .then(() => {
                fixture = TestBed.createComponent(MyLoginComponent);
                component = fixture.componentInstance;

                // Mock dos serviços
                _myAlertService = TestBed.get(MyAlertService);
                _myAuthService = TestBed.get(MyAuthService);
                _myMenuService = TestBed.get(MyMenuService);
                _myOverlayService = TestBed.get(MyOverlayService);
                _rota = TestBed.get(ActivatedRoute);
                router = TestBed.get(Router);

                // Spies
                spyOn(component, 'logout');
                spyOn(router, 'navigate');
                spyOn(_myOverlayService, 'ocultar');
                spyOn(_myAlertService, 'error');
            });
    }));

    it('Método "logout" deve ser executado na inicialização', () => {
        expect(component).toBeDefined();
        component.ngOnInit();
        expect(component.returnUrl).toBe('/dashboard');
        expect(component.logout).toHaveBeenCalled();
    });

    it('Método login - sucesso na autenticação', () => {
        expect(component).toBeDefined();
        component.ngOnInit();
        component.usuario.username = 'usuario';
        component.usuario.password = 'senha';
        component.login();
        expect(_myOverlayService.ocultar).toHaveBeenCalled();
        expect(router.navigate).toHaveBeenCalled();
    });

    it('Método login - erro na autenticação', () => {
        expect(component).toBeDefined();
        component.ngOnInit();
        component.usuario.username = 'usuarioErrado';
        component.usuario.password = 'senhaErrada';
        component.login();
        expect(_myOverlayService.ocultar).toHaveBeenCalled();
        expect(_myAlertService.error).toHaveBeenCalled();
    });
});
