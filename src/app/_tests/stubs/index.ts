// start:ng42.barrel
export * from './my-alert.stub.service';
export * from './my-auth.stub.service';
export * from './my-menu.stub.service';
export * from './my-overlay.stub.service';
export * from './my-http.stub.service';
export * from './router.stub.service';
// end:ng42.barrel

