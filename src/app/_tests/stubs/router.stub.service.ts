export class RouterStub {
    navigate(url: string[]) {
        return url;
    }
}

export class ActivatedRouteStub {
    snapshot = {
        queryParams: []
    };

    constructor() {
        this.snapshot.queryParams['returnUrl'] = '/dashboard';
    }
}
