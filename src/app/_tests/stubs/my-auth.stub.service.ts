import { Injectable } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/observable/of';

import { MyJwtHelper } from './../../_helpers/my-jwtHelper';
import { MyTokenService } from './../../_services/my-token.service';

import './../../_types/my_types';

@Injectable()
export class MyAuthServiceStub {

    _myTokenService = new MyTokenService();
    _myJwtHelper = new MyJwtHelper();
    token = '';
    retornoSucesso: ApiResponseObj;
    retornoErro: ApiResponseObj;

    constructor() {
        this.retornoSucesso = {
            body: {
                id: 1,
                username: 'usuario',
                firstname: '"Usuário',
                lastname: 'de Tal',
                id_orgao: 301,
                nome_orgao: 'CIASC'
            },
            message: 'Usuário autenticado com sucesso!',
            token: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJDZW50cm8gZGUgSW5mb3Jtw6F0aWNhIGUgQXV0b21hw6fDo28gZG8gRXN0YWRvIGRlIFNhbnRhIENhdGFyaW5hIiwiaWF0IjoxNTA2NTIxMDY0LCJleHAiOjE1MDY1MjIyNjEsImF1ZCI6Ind3dy5jaWFzYy5zYy5nb3YuYnIiLCJzdWIiOiJ0YXZhcmVzQGNpYXNjLnNjLmdvdi5iciIsImZpcnN0bmFtZSI6Ik1hcmNlbG8iLCJsYXN0bmFtZSI6IlRhdmFyZXMiLCJvcmdhbyI6IkNJQVNDIn0.JTed7v7IJKJa61JaQ6Bc_tOUcwtAOuxgkbaxCg5E1JM',
            status: 200
        }

        this.retornoErro = {
            body: null,
            message: 'Erro na autenticação. Verifique as informações.',
            token: '',
            status: 500
        };
    }

    login(username: string, password: string): Observable<boolean> {
        const payload = { username: username, password: password };
        return this.doLogin('/authenticate', payload)
        .map((response: ApiResponseObj) => {
            if (response.status === 200) {
                this._myTokenService.saveToken(response.token);
                return true;
            } else {
                // return false to indicate failed login
                return false;
            }
        });
    }

    doLogin(path: string, payload: any): Observable<ApiResponseObj> {
        if ((payload.username === 'usuario') && (payload.password === 'senha')) {
            return Observable.of(this.retornoSucesso);
        } else {
            return Observable.of(this.retornoErro);
        }
    }

    logout() {
        this._myTokenService.deleteToken();
    }

    loggedIn() {
        return (this._myTokenService.getToken()) ? true : false;
    }

    getNomeCompleto(first: string, last: string) {
        return first + ' ' + last;
    }

    getCurrentUser() {
        const token = this._myTokenService.getToken();

        if (token) {
            const decoded = this._myJwtHelper.decodeToken(token);

            const user = {
                username: decoded.username,
                nomecompleto: this.getNomeCompleto(decoded.firstname, decoded.lastname),
                orgao: decoded.orgao
            };
            return user;
        } else {
            return null;
        }
    }
}
