import { MyMenuService } from './../_services/index';

describe('MyMenuService: ', () => {

    let myMenuService: MyMenuService;
    let autenticado: boolean;

    beforeEach(() => {
        myMenuService = new MyMenuService();
    });

    it('Método "mostrar" deve provocar a exibição do menu', () => {
        expect(myMenuService).toBeDefined();
        myMenuService.getMessage()
            .subscribe(resultado => {
                autenticado = resultado;
                expect(autenticado).toBe(true);
            });

        myMenuService.mostrar();
    });

    it('Método "ocultar" deve provocar a exibição do menu', () => {
        expect(myMenuService).toBeDefined();
        myMenuService.getMessage()
            .subscribe(resultado => {
                autenticado = resultado;
                expect(autenticado).toBe(false);
            });

        myMenuService.ocultar();
    });

});
