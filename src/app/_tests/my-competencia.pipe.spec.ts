import {MyCompetenciaPipe} from "../_pipes/my-competencia.pipe";

describe('MyCompetenciaPipe: ', () => {

    let myCompetenciaPipe: MyCompetenciaPipe;
    let competencia: String;

    beforeEach(() => {
        myCompetenciaPipe = new MyCompetenciaPipe();
        competencia = '2018-06';
    });

    it('pipe deve retornar a competencia no formato MMM/YYYY', () => {
        expect(myCompetenciaPipe.transform(competencia)).toEqual('Jun/2018');
    });
});
