import { MyDatePipe } from './../_pipes/my-date.pipe';

describe('MyDatePipe: ', () => {

    let myDatePipe: MyDatePipe;
    let dataGeracao: Date;

    beforeEach(() => {
        myDatePipe = new MyDatePipe();
        dataGeracao = new Date('2017-09-03 01:03:02');
    });

    it('pipe deve retornar a data formatada como DD/MM/AAA HH:MM:SS', () => {
        expect(myDatePipe.transform(dataGeracao)).toEqual('03/09/2017 01:03:02');
    });
});
