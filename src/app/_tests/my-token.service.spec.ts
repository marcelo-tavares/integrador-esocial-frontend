import { MyTokenService } from './../_services/index';

describe('MyTokenService: ', () => {

    let myTokenService: MyTokenService;
    let tokenRecuperado: string;

    beforeEach(() => {
        myTokenService = new MyTokenService();
        localStorage.removeItem('token');
    });

    it('Método "saveToken" deve registrar o token na localStorage', () => {
        expect(myTokenService).toBeDefined();        
        myTokenService.saveToken('aaa.bbb.ccc');
        expect (localStorage['token']).toBe('aaa.bbb.ccc');
    });
    
    it('Método "getToken" deve recuperar o token salvo na localStorage', () => {
        myTokenService.saveToken('aaa.bbb.ccc');
        expect (localStorage['token']).toBe('aaa.bbb.ccc');
        tokenRecuperado = myTokenService.getToken();
        expect(tokenRecuperado).toBe('aaa.bbb.ccc');
    });

    it('Método "deleteToken" deve remover o token da localStorage', () => {
        myTokenService.saveToken('aaa.bbb.ccc');
        expect (localStorage['token']).toBe('aaa.bbb.ccc');
        myTokenService.deleteToken();
        expect(localStorage['token']).toBeUndefined();
    });
});
