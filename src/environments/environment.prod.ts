export const environment = {
    production: true,
    envName: 'prod',
    APP_CONFIG_IMPL: {
      BACKEND_URL: 'https://esocial.intranet.sc.gov.br:9443'
    }
  };
