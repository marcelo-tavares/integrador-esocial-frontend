export const environment = {
    production: false,
    envName: 'stag',
    APP_CONFIG_IMPL: {
      BACKEND_URL: 'https://esocialhom.intranet.ciasc.gov.br:9443'
    }
  };
