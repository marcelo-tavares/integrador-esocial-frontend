# Frontend
Projeto gerado com Angular-CLI. 

## Servidor de desenvolvimento
Executar `ng serve` para rodar um servidor web de desenvolvimento. 
Navegar `http://localhost:4200/`. 
A aplicação recarrega automaticamente quando algum arquivo de códifo fonte for alterado.


## Build

Execute `ng build --prod --build-optimizer` para fazer o build do projeto.
--prod fará com que seja usado o arquivo de ambiente de produção
--build-optimizer fará com que o tamanho dos arquivos gerados sejam menores.

Os artefatos da distribuiçao serão gravados na pasta `dist/`..
